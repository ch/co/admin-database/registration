"""
Defines differences between our roles and hence which forms are offered for the
role

The post_choices_hid allows us to reuse a basic form for different sets of categories but
right now we haven't any cases of this
"""
from collections import OrderedDict

roles = OrderedDict(
    [
        (
            "staff",
            {
                "form_title": "Academic, Academic-related, and Assistant Staff",
                "form_class": "StaffRegistrationForm",
                "post_choices_hid": "staff_type",
            },
        ),
        (
            "research",
            {
                "form_title": "Research staff",
                "form_class": "ResearcherRegistrationForm",
                "post_choices_hid": "researcher_type",
            },
        ),
        (
            "postgrad",
            {
                "form_title": "Postgraduate Students",
                "form_class": "PostGradRegistrationForm",
                "post_choices_hid": "postgrad_type",
                "help_text": "only for Cambridge Chemistry registered students",
            },
        ),
        (
            "erasmus",
            {
                "form_title": "Erasmus Students",
                "form_class": "ErasmusRegistrationForm",
                "post_choices_hid": "erasmus_type",
            },
        ),
        (
            "embedded_company_staff",
            {
                "form_title": "Embedded Company Staff",
                "form_class": "EmbeddedStaffRegistrationForm",
                "post_choices_hid": "embedded_staff_type",
                "help_text": "only for staff working for companies housed in the "
                "Department of Chemistry incubator space",
            },
        ),
        (
            "visitor",
            {
                "form_title": "Visitors",
                "form_class": "VisitorRegistrationForm",
                "post_choices_hid": "visitor_type",
                "help_text": "anyone not in another category, including PhD/MPhils registered "
                "in other departments, University staff not employed by Chemistry, people from "
                "other universities or companies",
            },
        ),
    ]
)
