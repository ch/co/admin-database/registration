"""
Defines the navigation page for the new starter
"""
from flask import Blueprint, redirect, render_template, request, url_for

from chemistry_starters import database, utils

starter_navigation = Blueprint("starter_navigation", __name__)


@starter_navigation.route("/")
def index():
    return redirect(url_for(".navigation_index", token=request.args.get("token")))


@starter_navigation.route("/register")
@utils.check_token()
def navigation_index():
    uuid, role = utils.decode_token()
    safety_signer_id = database.get_form_column(
        "safety_induction_person_signing_off_id", uuid
    )
    return render_template(
        "navigation/index.html",
        safety_training_html=database.get_safety_training_html_for_uuid(uuid),
        post_category=database.get_form_column("post_category_id", uuid),
        person_role_form_complete=utils.person_role_details.is_form_submitted(uuid),
        safety_checklist_complete=utils.safety.is_form_submitted(uuid),
        safety_checklist_checker=database.name_for_person(
            safety_signer_id, identifier_type="id"
        ),
        safety_check_token=utils.generate_token_for_form(
            form_uuid=uuid, form_type="safety_checklist", salt="safety_checklist"
        ),
        safety_checklist_signed=utils.safety.is_safety_checklist_signed(uuid),
        safety_training_signoff_needed=database.is_safety_training_signoff_needed(uuid),
        safety_training_signed=utils.safety.is_safety_training_signed(uuid),
        process_complete=utils.control.is_registration_complete(uuid, role=role),
    )
