"""
Define endpoints for showing examples of the forms the user has to fill in
"""

from flask import Blueprint, render_template, request, url_for

from chemistry_starters import app
from chemistry_starters.forms import static_forms

example_forms = Blueprint("example_forms", __name__)


student_safety_training_html = (
    '<a target="_blank" rel="noopen noreferer" '
    'href="https://www.vle.cam.ac.uk/course/view.php?id=220222">'
    "https://www.vle.cam.ac.uk/course/view.php?id=220222</a>"
    " and for experimentalists also "
    '<a target="_blank" rel="noopener noreferrer" '
    'href="https://www.vle.cam.ac.uk/course/view.php?id=252161">'
    "https://www.vle.cam.ac.uk/course/view.php?id=252161</a>"
)


@example_forms.route("/safety")
def example_safety_forms_index():
    """
    Generate an index page for all the example safety forms
    """
    examples = []
    prefix = request.url_rule.rule
    for rule in app.url_map.iter_rules():
        if rule.rule.startswith(prefix) and rule.rule != prefix:
            examples.append(url_for(rule.endpoint))
    examples.append(url_for("example_forms.example_safety_signoff"))
    return render_template("safety/examples.html", examples=examples)


@example_forms.route("/safety/without_training")
def example_safety_form():
    """
    Render a blank safety checklist form without safety training

    To let the safety team check if the checklist needs editing without having
    to go through a registration Does not need to be protected, although it
    probably will be because of the structure of the URL
    """
    from chemistry_starters.forms import safety_forms

    safety_form = safety_forms.SafetyChecklist()
    return render_template(
        "safety/safety_form.html",
        form=safety_form,
        starter_name="Example Person",
        disable_submission=True,
    )


@example_forms.route("/safety/with_student_training")
def example_student_safety_form():
    return example_safety_with_training()


@example_forms.route("/safety/with_nonstudent_training")
def example_non_student_safety_form():
    html = (
        '<a href="https://www.ch.cam.ac.uk/safety/example">'
        "https://www.ch.cam.ac.uk/safety/example"
        "</a>"
    )
    return example_safety_with_training(safety_training_html=html)


def example_safety_with_training(
    safety_training_html=student_safety_training_html,
):
    """
    Render a blank safety checklist

    To let the safety team check if the checklist needs editing without having
    to go through a registration Does not need to be protected, although it
    probably will be because of the structure of the URL
    """
    from chemistry_starters.forms import safety_forms

    safety_form = safety_forms.SafetyChecklistWithTraining()
    return render_template(
        "safety/safety_form.html",
        form=safety_form,
        starter_name="Example Person",
        disable_submission=True,
        safety_training_html=safety_training_html,
    )


@example_forms.route("/sign/safety")
def example_safety_signoff():
    safety_signoff_form = static_forms.SafetyInductionSignOffForm()
    return render_template(
        "safety/safety_checklist_sign.html",
        form=safety_signoff_form,
        starter_name="Example Starter",
        signer_name="Example Inductor",
        form_url=url_for(
            "static", filename="pdf/example-safety-checklist.pdf", _external=True
        ),
    )
