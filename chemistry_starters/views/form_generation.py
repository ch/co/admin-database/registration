"""
Defines the endpoints to do with generating forms
"""
from flask import (
    Blueprint,
    g,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
)

from chemistry_starters import app, database, utils
from chemistry_starters.forms import form_generation as forms
from chemistry_starters.roles import roles
from chemistry_starters.utils.comms import email_starter
from chemistry_starters.utils.form_generation import is_form_imported, save_initial_form
from chemistry_starters.utils.safety import is_safety_handbook_file_available

form_generation = Blueprint("form_generation", __name__)


@form_generation.route("/", methods=["GET", "POST"])
def forms_index():
    return redirect(url_for("form_generation.create_forms"))


@form_generation.route("/new", methods=["GET", "POST"])
@utils.check_acl(acl_view=app.config["FORM_GENERATION_ACL_VIEW"])
def create_forms():
    get_forms_form = forms.GenerateFormsForm(request.form)
    form_types = database.form_types_for_person(g.crsid)
    choices = []
    for role in roles:
        if role in form_types:
            if "help_text" in roles[role]:
                choices.append(
                    [role, f"{roles[role]['form_title']} - {roles[role]['help_text']}"]
                )
            else:
                choices.append([role, roles[role]["form_title"]])
    get_forms_form.form_type.choices = choices
    # get_hids' null_option's value is an int type by default, but in this case we
    # are coercing the value to a string as it comes back from the browser
    # so that always fails validation; we have to set a null value that is a string type.
    get_forms_form.post_category_id.choices = database.get_hids(
        "visitor_type", null_option="", null_value="v-0"
    )
    uuid = None
    url = None
    new_form = True
    if request.method == "POST" and get_forms_form.validate():
        # If there is an email given, check for an existing incomplete form with the email
        # and use that one if found, generating an updated token for it
        if get_forms_form.starter_email.data:
            existing_form_uuids = database.uuids_for_email(
                get_forms_form.starter_email.data
            )
            for current_uuid in existing_form_uuids:
                if not is_form_imported(current_uuid):
                    uuid = current_uuid
                    new_form = False
                    break
        token = utils.generate_token_for_form(
            request.form.get("form_type"), form_uuid=uuid
        )
        # Check for a null post_category
        post_category_id = get_forms_form.post_category_id.data
        if post_category_id == "v-0":
            post_category_id = None
        # ensure uuid is set as if it's a completely new form it won't be yet
        uuid, role = utils.decode_token("form", token)
        url = url_for(
            "starter_navigation.navigation_index", _external=True, token=token
        )
        save_initial_form(
            uuid,
            role,
            g.crsid,
            get_forms_form.starter_email.data,
            post_category_id=post_category_id,
        )
        if get_forms_form.starter_email.data:
            email_starter(get_forms_form.starter_email.data, url)
    return render_template(
        "form_generation/new_forms.html",
        url=url,
        form=get_forms_form,
        form_lifetime=app.config.get("FORM_LIFETIME_DAYS", 7),
        new_form=new_form,
        safety_handbook_available=is_safety_handbook_file_available(),
    )


@form_generation.route("/bulk", methods=["GET", "POST"])
@utils.check_acl(acl_view=app.config["FORM_GENERATION_ACL_VIEW"])
def create_forms_bulk():
    get_forms_form = forms.GenerateBulkFormsForm(request.form)
    form_types = database.form_types_for_person(g.crsid)
    get_forms_form.form_type.choices = [
        (x, roles[x]["form_title"]) for x in form_types if x in roles
    ]
    get_forms_form.post_category_id.choices = database.get_hids(
        "visitor_type", null_option="", null_value="v-0"
    )
    form_args = {
        "bulk_tokens": True,
        "form": get_forms_form,
        "form_lifetime": app.config.get("FORM_LIFETIME_DAYS", 7),
        "safety_handbook_available": is_safety_handbook_file_available(),
    }
    # FIXME validate form here or we get roles of None
    possible_emails = get_forms_form.starter_email.data
    if possible_emails and get_forms_form.validate():
        emails = [x.strip() for x in possible_emails.split("\n") if x.strip()]
        failed_emails = []
        for email in emails:
            uuid = None
            token = None
            # look for existing incomplete forms
            existing_form_uuids = database.uuids_for_email(email)
            for current_uuid in existing_form_uuids:
                if not is_form_imported(current_uuid):
                    uuid = current_uuid
                    break
            token = utils.generate_token_for_form(
                request.form.get("form_type"), form_uuid=uuid
            )
            # ensure uuid is set as it may not yet be
            uuid, role = utils.decode_token("form", token)
            url = url_for(
                "starter_navigation.navigation_index", _external=True, token=token
            )
            if get_forms_form.send_reminders.data is True:
                remind = g.crsid
            else:
                remind = None
            save_initial_form(uuid, role, remind, email)
            email_starter(email, url) or failed_emails.append(email)
            form_args["failed_emails"] = failed_emails
            form_args["url"] = True
        get_forms_form.starter_email.data = None
    return render_template("form_generation/new_forms.html", **form_args)


@form_generation.route("/safety-handbook", methods=["GET"])
@utils.check_acl(acl_view=app.config["FORM_GENERATION_ACL_VIEW"])
def safety_handbook():
    """
    Serve up the safety handbook

    This document is not in the static directory because it has to have access control.
    It must also not be committed to the code repository because the contents
    are secret.
    """
    return send_from_directory(
        app.root_path + "/documents/", app.config["SAFETY_HANDBOOK_FILENAME"]
    )
