"""
Routes for the static content
"""

from flask import Blueprint, send_from_directory

from chemistry_starters import app

static_content = Blueprint("static_content", __name__)


@static_content.route("/jquery/<path:filename>")
def jquery_static(filename):
    return send_from_directory(
        app.config.get("JQUERY_PATH", app.root_path + "/jquery/"), filename
    )


@static_content.route("/jquery-ui/<path:filename>")
def jquery_ui_static(filename):
    return send_from_directory(
        app.config.get("JQUERY_UI_PATH", app.root_path + "/jquery-ui/"), filename
    )


@static_content.route("/pl/<path:filename>")
def pl_static(filename):
    return send_from_directory(app.root_path + "/pl/", filename)
