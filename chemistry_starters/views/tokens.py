"""
To provide token management functions
"""

import uuid
from urllib.parse import parse_qs, urlparse

from flask import Blueprint, render_template, request, url_for
from itsdangerous import BadSignature

from chemistry_starters import app, utils
from chemistry_starters.forms import static_forms

token_management = Blueprint("token_management", __name__)


@token_management.route("/decode", methods=["GET", "POST"])
@utils.db_groups_only("cos")
def decode_token():
    decode_form = static_forms.GetToken(request.form)
    args = {
        "form": decode_form,
        "schema": app.config["DB_SCHEMA"],
        "table": app.config["FORM_TABLE"],
    }
    # decode, need to try all the likely salts
    if decode_form.validate_on_submit():
        try:
            old_token = parse_qs(urlparse(decode_form.token.data).query).get("token")[0]
        except TypeError:
            old_token = decode_form.token.data
        for salt in ["form", "safety_checklist", "rendered_safety_checklist"]:
            try:
                uuid_str, args["role"] = utils.decode_token(salt=salt, token=old_token)
                args["workingsalt"] = salt
                args["uuid"] = uuid.UUID(uuid_str)
            except BadSignature:  # do I need to check for SignatureExpired? no
                pass
        if "workingsalt" in args:
            args["days"] = utils.get_token_days_remaining(
                args["workingsalt"], old_token
            )
    # render template with form and uuid and role and workingsalt
    return render_template("tokens/decode.html", **args)


@token_management.route("/extend", methods=["GET", "POST"])
@utils.db_groups_only("cos", "hr", "student_management")
def extend_token():
    extend_form = static_forms.GetToken(request.form)
    args = {"form": extend_form}
    if extend_form.validate_on_submit():
        try:
            old_token = parse_qs(urlparse(extend_form.token.data).query).get("token")[0]
        except TypeError:
            old_token = extend_form.token.data
        for salt in ["form", "safety_checklist", "rendered_safety_checklist"]:
            try:
                uuid, role = utils.decode_token(salt=salt, token=old_token)
            except BadSignature:
                continue
            workingsalt = salt
        try:
            args["new_token"] = utils.generate_token_for_form(
                form_type=role, form_uuid=uuid, salt=workingsalt
            )
        except UnboundLocalError:
            args["message"] = "Could not decode this token"
    return render_template("tokens/extend.html", **args)


@token_management.route("/custom", methods=["GET", "POST"])
@utils.db_groups_only("cos")
def get_token_for():
    token_form = static_forms.GetTokenParams(request.form)
    args = {"form": token_form}
    if token_form.validate_on_submit():
        salt = token_form.salt.data
        token = utils.generate_token_for_form(
            form_type=token_form.role.data,
            form_uuid=token_form.uuid.data,
            salt=salt,
        )
        args["token"] = token
        if salt == "form":
            args["url"] = url_for(
                "starter_navigation.navigation_index",
                _external=True,
                token=args["token"],
            )
        elif salt == "safety_checklist":
            args["url"] = url_for(
                "safety_checklist.safety_checklist_signoff",
                _external=True,
                token=args["token"],
            )
        elif salt == "":
            args["url"] = url_for(
                "safety_checklist.get_completed_safety_form",
                _external=True,
                token=args["token"],
            )
    return render_template("tokens/custom_token.html", **args)


@token_management.route("/")
@utils.db_groups_only("cos", "hr", "student_management")
def tokens_index():
    """
    Generate an index page for all the token forms
    """
    links = []
    prefix = request.url_rule.rule
    for rule in app.url_map.iter_rules():
        if rule.rule.startswith(prefix) and rule.rule != prefix:
            links.append(url_for(rule.endpoint))
    return render_template("tokens/index.html", links=links)
