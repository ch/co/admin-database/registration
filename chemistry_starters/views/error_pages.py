"""
Define simple error pages used throughout the application
"""

from flask import Blueprint, render_template

from chemistry_starters import SecretKeyNotSet

error_pages = Blueprint("error_pages", __name__)


@error_pages.route("/forbidden")
def forbidden():
    return render_template("errors/forbidden.html"), 503


@error_pages.route("/unavailable")
def unavailable():
    return render_template("errors/unavailable.html"), 503


@error_pages.route("/form_not_found")
def form_not_found():
    return render_template("errors/no_such_form.html"), 404


@error_pages.route("/error")
def error():
    return render_template("errors/error.html"), 500


@error_pages.route("/expired")
def expired():
    return render_template("errors/expired.html"), 500


@error_pages.route("/safety_training_not_found")
def safety_training_not_found():
    return render_template("errors/safety_training_not_found.html"), 500


@error_pages.app_errorhandler(SecretKeyNotSet)
def no_secret_key(e):
    return render_template("errors/no_secret_key.html"), 500
