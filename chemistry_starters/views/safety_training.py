from flask import Blueprint, g, redirect, render_template, request, url_for
from flask_wtf import FlaskForm
from wtforms import BooleanField

from chemistry_starters import app, database, utils
from chemistry_starters.utils import control

safety_training = Blueprint("safety_training", __name__)


@safety_training.route("/sign/safety_training", methods=["GET", "POST"])
@utils.db_groups_only("student_management", "dev")
def safety_training_signoff():
    """
    Form for signing off safety training

    This process is done in batches, as one or two people handle it for all
    registrations, which is why we present a Raven-protected page rather than
    using tokens to allow people to sign off individuals.
    """
    # This is needed so we can build this form dynamically
    # based on what we get back from the database
    class SafetyTrainingSignOff(FlaskForm):
        pass

    # Define the form field for each person
    for u in utils.safety.get_forms_awaiting_training_signoff():
        setattr(SafetyTrainingSignOff, u, BooleanField(database.index_name_for_uuid(u)))
    # Instantiate the form
    training_form = SafetyTrainingSignOff(request.form)
    if request.method == "POST":
        if not training_form.validate():
            app.logger(
                "Could not validate training check form {}".format(training_form.errors)
            )
            return redirect(url_for("error_pages.error"))
        for field in training_form:
            if field.type == "BooleanField" and field.data:
                utils.safety.set_safety_training_signed(field.name, g.crsid)
                if control.is_registration_complete(field.name):
                    control.send_registration_to_admin_team(field.name)
        return redirect(url_for(".safety_training_signoff"))
    return render_template("safety/safety_training_signoff.html", form=training_form)
