"""
Defines the application's endpoints
"""
import datetime

from flask import (
    Blueprint,
    g,
    redirect,
    render_template,
    request,
    send_file,
    send_from_directory,
    url_for,
)

from chemistry_starters import app, database, utils
from chemistry_starters.forms import static_forms
from chemistry_starters.utils import comms, control
from chemistry_starters.utils.safety import is_safety_handbook_file_available

safety_checklist = Blueprint("safety_checklist", __name__)


@safety_checklist.route("/register/safety", methods=["GET", "POST"])
def redirect_safety():
    return redirect(url_for(".safety_checklist_form", token=request.args.get("token")))


@safety_checklist.route("/register/safety_checklist", methods=["GET", "POST"])
@utils.check_token()
def safety_checklist_form():
    uuid, role = utils.decode_token()
    try:
        (
            form_data,
            safety_already_submitted,
            main_form_submitted,
        ) = utils.safety.load_safety_form(uuid)
    except database.DatabaseNotAvailable:
        return redirect(url_for("error_pages.unavailable"))
    starter_name = database.name_for_uuid(uuid)
    if not main_form_submitted:
        return redirect(
            url_for(
                "person_role_details.person_role_details_form",
                token=request.args.get("token"),
            )
        )
    if safety_already_submitted:
        return redirect(
            url_for(
                "starter_navigation.navigation_index", token=request.args.get("token")
            )
        )
    safety_training_html = database.get_safety_training_html_for_uuid(uuid)

    from chemistry_starters.forms import safety_forms as forms

    if form_data is None:
        form_data = {}
    if safety_training_html is None:
        safety_form = forms.SafetyChecklist(data=form_data)
    else:
        safety_form = forms.SafetyChecklistWithTraining(data=form_data)
    if request.method == "GET":
        return render_template(
            "safety/safety_form.html",
            form=safety_form,
            starter_name=starter_name,
            safety_training_html=safety_training_html,
            safety_handbook_available=is_safety_handbook_file_available(),
        )
    else:
        if request.form.get("submit") != "Check data and submit form":
            safety_form.safety_submitted.data = False
            utils.safety.save_safety_form_state(uuid, safety_form)
            return render_template(
                "safety/safety_form.html",
                form=safety_form,
                starter_name=starter_name,
                safety_training_html=safety_training_html,
                safety_handbook_available=is_safety_handbook_file_available(),
            )
        else:
            if safety_form.validate_on_submit():
                utils.safety.save_safety_form_state(uuid, safety_form)
                worked = utils.safety.save_rendered_safety_form(
                    uuid, safety_form, starter_name
                )
                if worked:
                    utils.mail_safety_induction_signoff(uuid)
                    comms.mail_form_creator_on_progress(uuid)
                    return redirect(
                        url_for(
                            "starter_navigation.navigation_index",
                            token=request.args.get("token"),
                        )
                    )
                else:
                    return render_template("safety/safety_failed_to_save.html"), 500
            else:
                safety_submit_box_state = safety_form.safety_submitted.data
                safety_form.safety_submitted.data = False
                utils.safety.save_safety_form_state(uuid, safety_form)
                # Reset the submitted flag for the UI
                safety_form.safety_submitted.data = safety_submit_box_state
                return render_template(
                    "safety/safety_form.html",
                    incomplete=True,
                    form=safety_form,
                    starter_name=starter_name,
                    safety_training_html=safety_training_html,
                    safety_handbook_available=is_safety_handbook_file_available(),
                )


@safety_checklist.route("/sign/safety", methods=["GET", "POST"])
def redirect_safety_signer():
    return redirect(
        url_for(".safety_checklist_signoff", token=request.args.get("token"))
    )


@safety_checklist.route("/sign/safety_checklist", methods=["GET", "POST"])
@utils.check_acl()
@utils.check_token(salt="safety_checklist")
def safety_checklist_signoff():
    """
    To allow a group member to sign off a safety checklist
    """
    uuid, role = utils.decode_token(salt="safety_checklist")
    # load the uuid and check form has not been signed
    try:
        person_to_sign = utils.safety.get_safety_checklist_signer_crsid(uuid)
        if person_to_sign != g.crsid:
            return render_template("safety/wrong_safety_signer.html"), 503
        already_signed = utils.safety.is_safety_checklist_signed(uuid)
        if already_signed:
            return render_template(
                "safety/safety_checklist_signed_off.html",
                starter_name=database.name_for_uuid(uuid),
                signer_name=database.name_for_person(g.crsid),
                registration_complete=control.is_registration_complete(uuid),
            )
    except database.DatabaseNotAvailable:
        return redirect(url_for("error_pages.unavailable"))
    safety_signoff_form = static_forms.SafetyInductionSignOffForm()
    # if GET offer up the form
    if (request.method == "GET") or (not safety_signoff_form.validate_on_submit()):
        # Generate a token for fetching the PDF form
        checklist_token = utils.generate_token_for_form(
            form_type="rendered_safety_checklist",
            form_uuid=uuid,
            salt="rendered_safety_checklist",
        )
        return render_template(
            "safety/safety_checklist_sign.html",
            form=safety_signoff_form,
            starter_name=database.name_for_uuid(uuid),
            signer_name=database.name_for_person(g.crsid),
            form_url=url_for(
                ".get_completed_safety_form", _external=True, token=checklist_token
            ),
        )
    else:
        database.save_form_column(
            "safety_induction_signed_off_date", datetime.date.today(), uuid
        )
        if control.is_registration_complete(uuid):
            utils.control.send_registration_to_admin_team(uuid)
        return render_template(
            "safety/safety_checklist_signed_off.html",
            starter_name=database.name_for_uuid(uuid),
            signer_name=database.name_for_person(g.crsid),
            registration_complete=control.is_registration_complete(uuid),
        )


@safety_checklist.route("/forms/completed/safety")
@utils.check_acl()
@utils.check_token(salt="rendered_safety_checklist")
def get_completed_safety_form():
    uuid, _ = utils.decode_token(salt="rendered_safety_checklist")
    try:
        if utils.safety.check_safety_checklist_access(uuid):
            memory_file = utils.safety.get_rendered_safety_checklist(uuid)
            return send_file(
                memory_file, as_attachment=True, download_name="checklist.pdf"
            )
        else:
            return redirect(url_for("error_pages.forbidden"))
    except database.DatabaseNotAvailable:
        return redirect(url_for("error_pages.unavailable"))


@safety_checklist.route("/register/safety-handbook")
@utils.check_token()
def safety_handbook():
    """
    Serve up the safety handbook

    This document is not in the static directory because it has to have access control.
    It must also not be committed to the code repository because the contents
    are secret. This route to download it is protected by token checking.
    """
    return send_from_directory(
        app.root_path + "/documents/", app.config["SAFETY_HANDBOOK_FILENAME"]
    )
