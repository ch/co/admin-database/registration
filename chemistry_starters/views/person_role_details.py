"""
Defines the main registration form, for people's personal and role details
"""
from flask import Blueprint, redirect, render_template, request, url_for

from chemistry_starters import database, utils

person_role_details = Blueprint("person_role_details", __name__)


@person_role_details.route("/register/main", methods=["GET", "POST"])
@utils.check_token()
def person_role_details_form():
    uuid, role = utils.decode_token()
    days_left = utils.get_token_days_remaining(
        salt="form", token=request.args.get("token")
    )
    try:
        form_data = utils.person_role_details.load_main_form(uuid)
    except database.DatabaseNotAvailable:
        return redirect(url_for("error_pages.unavailable"))
    if form_data is None:  # this form has been completed, return to navigation page
        return redirect(
            url_for(
                "starter_navigation.navigation_index", token=request.args.get("token")
            )
        )
    form_data["role"] = role
    form_data["uuid"] = uuid
    form_data["days_left"] = days_left
    if "email" in form_data:
        form_data["email2"] = form_data["email"]
    # This takes care of setting up the appropriate WTForms object type based on role
    template_kwargs = utils.person_role_details.registration_template_factory(
        **form_data
    )
    registration_form = template_kwargs["form"]
    # Save state in database even if it's not validated - the data coercion settings
    # should avoid SQL bombing out
    if request.form.get("submit") != "Submit this form and return to task list":
        # Just saving state, not submitting the application
        registration_form.submitted.data = False
        utils.person_role_details.save_main_form(uuid, role, registration_form)
        return render_template("main_registration/register.html", **template_kwargs)
    else:
        if registration_form.validate_on_submit():
            utils.person_role_details.save_main_form(uuid, role, registration_form)
            # Could check for whole process being complete here, although currently there is always
            # a second step
            return redirect(
                url_for(
                    "starter_navigation.navigation_index",
                    token=request.args.get("token"),
                )
            )
        else:
            # Tried to submit but it wasn't validated.
            # Instead save the state in the db without the submitted flag
            save_submit_box_state = registration_form.submitted.data
            registration_form.submitted.data = False
            utils.person_role_details.save_main_form(uuid, role, registration_form)
            # Reset the submitted flag for the UI
            registration_form.submitted.data = save_submit_box_state
            template_kwargs["incomplete"] = True
            return render_template("main_registration/register.html", **template_kwargs)
