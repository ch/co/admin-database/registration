import datetime

import psycopg2
import psycopg2.extras
import psycopg2.sql as sql
from chemaccmgmt import db
from chemaccmgmt.utils import db_email_to_array
from flask import g

from chemistry_starters import app


class DatabaseNotAvailable(Exception):
    pass


def get_db():
    """
    Check for a database connection and create one if there's none
    """
    chemdb = getattr(g, "_database", None)
    if chemdb is None:
        try:
            chemdb = db.ChemDB(username="starters_registration", use_pgpass=True)
        except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
            app.logger.error(
                "Failed to connect to database, error was {}".format(str(e))
            )
            raise DatabaseNotAvailable
        g._database = chemdb
    return chemdb


def get_hids(tablestem, null_option=None, null_value=-1):
    """
    Get a list of human-readable-identifier, primary key pairs from the database for a table

    null_option adds an option which translates to null in the database
    null_value is the value to set for it
    """
    tablename = tablestem + "_hid"
    db = get_db()
    query = sql.SQL("""SELECT id, hid FROM {schema}.{table}""").format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]), table=sql.Identifier(tablename)
    )
    db.cursor.execute(query)
    hids = db.cursor.fetchall()
    if null_option is not None:
        # We need a 'no data' value. 'None' is easily confused with Python None,
        # but NULL implies SQL NULL; compromising on -1 as the default for null_value
        hids.insert(0, (null_value, null_option))
    db.conn.rollback()
    return hids


def name_for_uuid(uuid):
    """
    Returns the name that the database has for a particular form UUID

    If there isn't one, returns None
    """
    db = get_db()
    query = sql.SQL(
        """
    SELECT COALESCE(first_names||' ','') || surname
    FROM {schema}.{table}
    LEFT JOIN {schema}.title_hid ON title_hid.id = {table}.title_id WHERE uuid = %s;
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    try:
        db.cursor.execute(query, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up name for uuid {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
        return None


def index_name_for_uuid(uuid):
    """
    Return a string to use to represent the name in an index for this uuid

    If there isn't one, returns None
    """
    db = get_db()
    query = sql.SQL(
        """SELECT surname
                              || ', '
                              || COALESCE(first_names,'')
                              || COALESCE(($$ ($$
                                           ||(CASE WHEN LENGTH(crsid) <> 0 THEN crsid ELSE NULL END)
                                           ||$$)$$)
                                          ,$$$$)
                       FROM {schema}.{table}
                       WHERE uuid = %s"""
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    try:
        db.cursor.execute(query, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up index name for uuid {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
        return None


def email_for_uuid(uuid):
    """
    Returns the email that the database has for a particular form UUID

    If there isn't one, returns None
    """
    db = get_db()
    query = sql.SQL(
        """
    select email from {schema}.{table} where uuid = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    try:
        db.cursor.execute(query, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up email for uuid {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
        return None


def email_for_current_person_id(person_id):
    """
    Return list of email addresses for registered person with given database id
    """
    email_query = sql.SQL(
        """
    SELECT person.email_address
    FROM {schema}.{crsids_view} person
    WHERE id = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        crsids_view=sql.Identifier(app.config["CURRENT_CRSIDS_VIEW"]),
    )
    db = get_db()
    try:
        db.cursor.execute(email_query, [person_id])
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            """Failed to connect to database to look up email for id {},
            error was {}""".format(
                person_id, str(e)
            )
        )
        db.conn.rollback()
        return False
    if db.cursor.rowcount == 1:
        # Flatten address list because the database contains columns with multiple addresses
        emails = [
            addr
            for email_col in db.cursor.fetchone()
            for addr in db_email_to_array(email_col)
        ]
    return emails


def uuids_for_email(email):
    """
    Returns list of UUIDs of any forms the database has with 'email'

    If there are none return empty array
    """
    db = get_db()
    query = sql.SQL(
        """
    select uuid from {schema}.{table} where email = lower(%s)
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    try:
        db.cursor.execute(query, [email])
        return [u[0] for u in db.cursor.fetchall()]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up uuids for email {}, error was {}".format(
                email, str(e)
            )
        )
        db.conn.rollback()
        return []


def name_for_person(identifier, identifier_type="crsid"):
    """
    Returns the name the database has for a crsid or id out of the list of _current_ people, or None

    Used for UI around getting Safety signed off. It does not look up names for crsids that people
    have put on registration forms.
    """
    db = get_db()
    query = sql.SQL(
        """
    SELECT name from {schema}.{view} where {identifier} = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["CURRENT_CRSIDS_VIEW"]),
        identifier=sql.Identifier(identifier_type),
    )
    try:
        db.cursor.execute(query, [identifier])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except psycopg2.ProgrammingError as e:
        app.logger.error(
            "Could not look up name for person with {} {}, error was {}".format(
                identifier_type, identifier, str(e)
            )
        )
        db.conn.rollback()
        return None


def id_for_crsid(crsid):
    """
    Returns the id the database has for a crsid out of the list of _current_ people, or None

    Used for recording who created a registration form.
    """
    db = get_db()
    query = sql.SQL(
        """
    SELECT id from {schema}.{view} where crsid = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["CURRENT_CRSIDS_VIEW"]),
    )
    try:
        db.cursor.execute(query, [crsid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except psycopg2.ProgrammingError as e:
        app.logger.error(
            "Could not look up id for person with crsid {}, error was {}".format(
                crsid, str(e)
            )
        )
        db.conn.rollback()
        return None


def get_form_column(column, uuid):
    """
    Return the value of column for uuid
    """
    signer_sql = sql.SQL(
        """
    SELECT {column} FROM {schema}.{table}
    WHERE uuid = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
        column=sql.Identifier(column),
    )
    db = get_db()
    try:
        db.cursor.execute(signer_sql, [uuid])
    except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
        app.logger.error(
            "Failed to look up column {} for uuid {}, error was {}".format(
                column, uuid, e
            )
        )
        db.conn.rollback()
        return None
    if db.cursor.rowcount == 1:
        return db.cursor.fetchone()[0]
    return None


def save_form_column(column, data, uuid):
    """
    Save a single column for a UUID to the database

    Used for saving processed safety data. The main registration data is saved
    by SQLSoup instead of this function, as that is all coerced to Postgres-friendly types
    by WTForms before we even see it. This function is only for data that we have to post-process.
    """
    save_sql = sql.SQL(
        """
        UPDATE {schema}.{table} SET {column} = %s WHERE uuid = %s
        """
    )
    db = get_db()
    try:
        db.cursor.execute(
            save_sql.format(
                schema=sql.Identifier(app.config["DB_SCHEMA"]),
                table=sql.Identifier(app.config["FORM_TABLE"]),
                column=sql.Identifier(column),
            ),
            [data, uuid],
        )
        db.conn.commit()
    except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
        app.logger.error(
            "Failed to save column {} to database for UUID {}, error was {}".format(
                column, uuid, str(e)
            )
        )
        db.conn.rollback()


def get_role_members(*roles):
    """
    Return a list of members of the database roles passed
    """
    roles_list = []
    query = sql.SQL(
        """SELECT DISTINCT member
                       FROM {schema}.{view}
                       WHERE """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["ROLES_VIEW"]),
    )
    where = sql.SQL(" OR ").join(
        [(sql.SQL("role = ") + sql.Literal(role)) for role in roles]
    )
    allowed_users_sql = query + where
    db = get_db()
    try:
        db.cursor.execute(allowed_users_sql)
        roles_list = [x[0] for x in db.cursor.fetchall()]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to look up roles in database for access control, error was {}".format(
                str(e)
            )
        )
        db.conn.rollback()
    return roles_list


def get_safety_training_html_for_uuid(uuid):
    """
    Return HTML describing the necessary safety training for a uuid, or None if
    we can't determine it.
    """
    db = get_db()
    query = sql.SQL(
        """SELECT safety_training_html
             FROM {schema}.{form}
             JOIN {schema}.{safety_training} ON {safety_training}.id = {form}.post_category_id
             WHERE form.uuid = %s"""
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        form=sql.Identifier(app.config["FORM_TABLE"]),
        safety_training=sql.Identifier(app.config["SAFETY_TRAINING_VIEW"]),
    )
    try:
        db.cursor.execute(query, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to look up safety training html in database, error was {}".format(
                str(e)
            )
        )
        db.conn.rollback()
    return None


def is_safety_training_signoff_needed(uuid):
    """
    Return boolean or None if we can't work it out
    """
    db = get_db()
    query = sql.SQL(
        """SELECT check_safety_training
             FROM {schema}.{form}
             JOIN {schema}.{safety_training} ON {safety_training}.id = {form}.post_category_id
             WHERE form.uuid = %s"""
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        form=sql.Identifier(app.config["FORM_TABLE"]),
        safety_training=sql.Identifier(app.config["SAFETY_TRAINING_VIEW"]),
    )
    try:
        db.cursor.execute(query, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()[0]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to look up safety training check in database, error was {}".format(
                str(e)
            )
        )
        db.conn.rollback()
    return None


def form_types_for_person(crsid):
    """
    Return a list of roles this crsid can generate forms for
    """
    form_types = []
    db = get_db()
    query = sql.SQL(
        """SELECT form_type FROM {schema}.{view}
           WHERE crsid = %s
        """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["FORM_GENERATION_ACL_VIEW"]),
    )
    try:
        db.cursor.execute(query, [crsid])
        form_types = [x[0] for x in db.cursor.fetchall()]
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to look up safety training check in database, error was {}".format(
                str(e)
            )
        )
        db.conn.rollback()
    return form_types


def get_incomplete_form_reminders(after_days=[7, 14]):
    """
    Return rows with data to send reminders about incomplete forms

    after_days is a list of the different numbers of days to remind after.
    keys in rows returned: reason, uuid, form_role, starter_email, form_age_days, admin_email
    """
    db = get_db()
    reminders = []
    query = sql.SQL(
        """SELECT
              uuid,
              form_role,
              starter_email,
              admin_email,
              current_date - _form_started AS form_age_days,
              reason
           FROM {schema}.{view}
           WHERE ({dates}) AND
           form_role IS NOT NULL"""
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["REMINDERS_VIEW"]),
        dates=sql.SQL(" OR ").join(
            [sql.SQL("(_form_started = %s)") for x in after_days]
        ),
    )
    try:
        db.cursor.execute(
            query,
            [datetime.date.today() - datetime.timedelta(days) for days in after_days],
        )
        reminders = db.cursor.fetchall()
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error("Failed to look up reminder data, error was {}".format(str(e)))
        db.conn.rollback()
    return reminders
