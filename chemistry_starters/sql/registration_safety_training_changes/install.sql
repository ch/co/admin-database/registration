ALTER TABLE registration.form ADD COLUMN "safety_training_needs_signoff" BOOLEAN;
ALTER TABLE registration.form ADD COLUMN "safety_training_person_signing_off_id" BIGINT;
ALTER TABLE registration.form ADD COLUMN "safety_training_signed_off_date" DATE;

ALTER TABLE registration.form ADD CONSTRAINT "form_safety_training_person_signing_off_id_fkey" FOREIGN KEY ("safety_training_person_signing_off_id") REFERENCES person(id);

CREATE VIEW hotwire3.safety_training_person_signing_off_hid AS
SELECT person_hid.person_id AS safety_training_person_signing_off_id,
       person_hid.person_hid AS safety_training_person_signing_off_hid
FROM hotwire3.person_hid;
ALTER TABLE hotwire3.safety_training_person_signing_off_hid OWNER TO dev;
GRANT SELECT ON hotwire3.safety_training_person_signing_off_hid TO ro_hid;

DROP VIEW hotwire3."10_View/People/Registration";
CREATE OR REPLACE VIEW hotwire3."10_View/People/Registration"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id, -- FIXME need hid view
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form AS ro_separate_safety_form,
    form.hide_from_website,
    form.previously_registered,
        CASE
            WHEN form._match_to_person_id IS NOT NULL THEN form._match_to_person_id
            ELSE registration.match_existing_person(form.surname::character varying, form.first_names::character varying, form.crsid, form.date_of_birth)
        END AS ro_automatically_matched_person_id,
    form._match_to_person_id AS match_to_person_id,
    false AS import_this_record,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE form.submitted = true AND form._imported_on IS NULL AND (form.separate_safety_form = true OR form.safety_induction_signed_off_date <= 'now'::text::date) AND ( ( form.safety_training_needs_signoff AND form.safety_training_signed_off_date <= 'now'::text::date ) OR (form.safety_training_needs_signoff IS DISTINCT FROM 't'));

ALTER TABLE hotwire3."10_View/People/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Registration" TO dev;
GRANT SELECT, UPDATE, DELETE ON TABLE hotwire3."10_View/People/Registration" TO hr;

CREATE OR REPLACE RULE hotwire3_view_registration_del AS
    ON DELETE TO hotwire3."10_View/People/Registration"
    DO INSTEAD
(DELETE FROM registration.form
  WHERE (form.form_id = old.id));

CREATE TRIGGER registration_form_update
    INSTEAD OF UPDATE
    ON hotwire3."10_View/People/Registration"
    FOR EACH ROW
    EXECUTE PROCEDURE registration.people_registration_upd();


DROP VIEW hotwire3."10_View/Safety/Registration";
CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Registration"
 AS
 SELECT form.form_id AS id,
    COALESCE(person.first_names, form.first_names::character varying) AS first_names,
    COALESCE(person.surname, form.surname::character varying) AS surname,
    COALESCE(person.known_as, form.known_as::character varying) AS known_as,
    COALESCE(person.title_id, form.title_id) AS title_id,
    COALESCE(_latest_role.post_category_id, form.post_category_id) AS post_category_id,
    COALESCE(ARRAY( SELECT mm_person_room.room_id
           FROM mm_person_room
          WHERE person.id = mm_person_room.person_id), form.dept_room_id) AS room_id,
    COALESCE(ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE person.id = mm_person_dept_telephone_number.person_id), form.dept_telephone_id) AS dept_telephone_number_id,
    COALESCE(person.email_address, form.email::character varying) AS email,
    COALESCE(person.crsid, form.crsid) AS crsid,
    COALESCE(person.arrival_date, form.start_date) AS arrival_date,
    COALESCE(_latest_role.intended_end_date, form.intended_end_date) AS intended_end_date,
    COALESCE(_latest_role.supervisor_id, form.department_host_id) AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.separate_safety_form,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    safety_induction_carried_out_by.person_hid AS ro_safety_induction_carried_out_by,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_off_on,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN hotwire3.person_hid safety_induction_carried_out_by ON form.safety_induction_person_signing_off_id = safety_induction_carried_out_by.person_id
     LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
  WHERE form.submitted = true;

ALTER TABLE hotwire3."10_View/Safety/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Registration" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Registration" TO safety_management;


DROP VIEW hotwire3."10_View/People/Processed_Registration_Forms";

CREATE OR REPLACE VIEW hotwire3."10_View/People/Processed_Registration_Forms"
 AS
 SELECT form.form_id AS id,
    form.first_names::character varying AS first_names,
    form.surname::character varying AS surname,
    form.known_as::character varying AS known_as,
    form.title_id,
    form.date_of_birth,
    form.gender_id,
    form.post_category_id,
    form.nationality_id,
    form.job_title::character varying AS job_title,
    form.dept_room_id AS room_id,
    form.dept_telephone_id AS dept_telephone_number_id,
    form.college_id AS cambridge_college_id,
    form.home_address,
    form.home_phone_number::character varying AS home_phone_number,
    form.mobile_number::character varying AS mobile_number,
    form.email::character varying AS email,
    form.crsid,
    form.start_date,
    form.intended_end_date,
    form.home_institution::character varying AS home_institution,
    form.department_host_id AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.deposit_receipt::character varying AS deposit_receipt,
    form.emergency_contact_name_1,
    form.emergency_contact_number_1,
    form.emergency_contact_name_2,
    form.emergency_contact_number_2,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    form.safety_induction_person_signing_off_id AS ro_safety_induction_signed_by_id,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_date,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date,
    form.separate_safety_form,
    form.hide_from_website,
    form.previously_registered,
    form._match_to_person_id AS match_to_person_id,
    form._imported_on AS ro_imported_on,
    hotwire3.to_hwsubviewb('10_View/People/_registration_matches'::character varying, 'id'::character varying, '10_View/People/Personnel_Data_Entry'::character varying, '_matched_person_id'::character varying, NULL::character varying) AS matched_person_details
   FROM registration.form
  WHERE form._imported_on IS NOT NULL;

ALTER TABLE hotwire3."10_View/People/Processed_Registration_Forms"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/People/Processed_Registration_Forms" TO hr;

DROP VIEW hotwire3."10_View/Safety/Registration";

CREATE OR REPLACE VIEW hotwire3."10_View/Safety/Registration"
 AS
 SELECT form.form_id AS id,
    COALESCE(person.first_names, form.first_names::character varying) AS first_names,
    COALESCE(person.surname, form.surname::character varying) AS surname,
    COALESCE(person.known_as, form.known_as::character varying) AS known_as,
    COALESCE(person.title_id, form.title_id) AS title_id,
    COALESCE(_latest_role.post_category_id, form.post_category_id) AS post_category_id,
    COALESCE(ARRAY( SELECT mm_person_room.room_id
           FROM mm_person_room
          WHERE person.id = mm_person_room.person_id), form.dept_room_id) AS room_id,
    COALESCE(ARRAY( SELECT mm_person_dept_telephone_number.dept_telephone_number_id
           FROM mm_person_dept_telephone_number
          WHERE person.id = mm_person_dept_telephone_number.person_id), form.dept_telephone_id) AS dept_telephone_number_id,
    COALESCE(person.email_address, form.email::character varying) AS email,
    COALESCE(person.crsid, form.crsid) AS crsid,
    COALESCE(person.arrival_date, form.start_date) AS arrival_date,
    COALESCE(_latest_role.intended_end_date, form.intended_end_date) AS intended_end_date,
    COALESCE(_latest_role.supervisor_id, form.department_host_id) AS supervisor_id,
    form.mifare_access_level_id,
    form.mifare_areas AS group_areas,
    form.separate_safety_form,
    'Content-type: application/pdf\012\012'::bytea || form.safety_form_pdf AS "safety_form.pdf",
    safety_induction_carried_out_by.person_hid AS ro_safety_induction_carried_out_by,
    form.safety_induction_signed_off_date AS ro_safety_induction_signed_off_on,
    form.safety_training_needs_signoff AS ro_safety_training_needs_signoff,
    form.safety_training_person_signing_off_id AS ro_safety_training_person_signing_off_id,
    form.safety_training_signed_off_date AS ro_safety_training_signed_off_date
   FROM registration.form
     LEFT JOIN person ON form._match_to_person_id = person.id
     LEFT JOIN hotwire3.person_hid safety_induction_carried_out_by ON form.safety_induction_person_signing_off_id = safety_induction_carried_out_by.person_id
     LEFT JOIN _latest_role_v12 _latest_role ON _latest_role.person_id = person.id
  WHERE form.submitted = true;

ALTER TABLE hotwire3."10_View/Safety/Registration"
    OWNER TO dev;

GRANT ALL ON TABLE hotwire3."10_View/Safety/Registration" TO dev;
GRANT SELECT ON TABLE hotwire3."10_View/Safety/Registration" TO safety_management;


CREATE VIEW registration.researcher_type_hid AS
SELECT 'sc-'::text || staff_category.id AS id,
    staff_category.category AS hid
       FROM staff_category
         WHERE staff_category.category::text = 'PDRA';

ALTER VIEW registration.researcher_type_hid OWNER TO dev;
GRANT SELECT ON registration.researcher_type_hid TO starters_registration;
