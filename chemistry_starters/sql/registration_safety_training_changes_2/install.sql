ALTER TABLE public.staff_category ADD COLUMN safety_training_url text;
ALTER TABLE public.staff_category ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.visitor_type_hid ADD COLUMN safety_training_url text;
ALTER TABLE public.visitor_type_hid ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.postgraduate_studentship_type ADD COLUMN safety_training_url text;
ALTER TABLE public.postgraduate_studentship_type ADD COLUMN check_safety_training boolean default false;

ALTER TABLE public.erasmus_type_hid ADD COLUMN safety_training_url text;
ALTER TABLE public.erasmus_type_hid ADD COLUMN check_safety_training boolean default false;

-- now set all the URLs

-- staff
UPDATE public.staff_category SET safety_training_url = 'academic' WHERE category = 'Academic staff';
-- nothing for assistant and acad-related
UPDATE public.staff_category SET safety_training_url = 'pdra' WHERE category = 'PDRA';

-- visitors
-- academic visitors
UPDATE public.visitor_type_hid SET safety_training_url = 'academic' WHERE visitor_type_hid = 'Visitor (Academic)';
-- student visitors
UPDATE public.visitor_type_hid SET safety_training_url = 'https://www.vle.cam.ac.uk/course/view.php?id=220222' WHERE visitor_type_hid = 'Visitor (PhD Postgraduate)' OR visitor_type_hid = 'Visitor (MPhil Postgraduate)';
UPDATE public.visitor_type_hid SET check_safety_training = 't' WHERE visitor_type_hid = 'Visitor (PhD Postgraduate)' OR visitor_type_hid = 'Visitor (MPhil Postgraduate)';
-- these two may need updating as RMT and AW disagree about training needed
UPDATE public.visitor_type_hid SET safety_training_url = 'https://www.vle.cam.ac.uk/course/view.php?id=220222' WHERE visitor_type_hid = 'Visitor (student)' OR visitor_type_hid = 'Summer Student';
UPDATE public.visitor_type_hid SET check_safety_training = 't' WHERE visitor_type_hid = 'Visitor (student)' OR visitor_type_hid = 'Summer Student';
-- pdra visitors
UPDATE public.visitor_type_hid SET safety_training_url = 'visitor_pdra' WHERE visitor_type_hid = 'Visitor (PDRA)';
-- nothing for embedded company staff/commercial visitors

-- no where clause on pg/erasmus because all students types do training and all are checked
UPDATE postgraduate_studentship_type SET safety_training_url = 'https://www.vle.cam.ac.uk/course/view.php?id=220222';
UPDATE postgraduate_studentship_type SET check_safety_training = 't';
UPDATE erasmus_type_hid SET safety_training_url = 'https://www.vle.cam.ac.uk/course/view.php?id=220222';
UPDATE erasmus_type_hid SET check_safety_training = 't';

-- create a view to list them all
CREATE VIEW registration.safety_training AS
SELECT
'e-'::text || erasmus_type_hid.erasmus_type_id::text AS id,
safety_training_url,
check_safety_training
FROM public.erasmus_type_hid
UNION
SELECT 'v-'::text || visitor_type_hid.visitor_type_id::text AS id,
safety_training_url,
check_safety_training
FROM public.visitor_type_hid
UNION
SELECT 'pg-'::text || postgraduate_studentship_type.id::text AS id,
safety_training_url,
check_safety_training
FROM public.postgraduate_studentship_type
UNION
SELECT 'sc-'::text || staff_category.id::text AS id,
  safety_training_url,
  check_safety_training
FROM public.staff_category
;

ALTER VIEW registration.safety_training OWNER TO dev;
GRANT SELECT ON registration.safety_training TO starters_registration;
