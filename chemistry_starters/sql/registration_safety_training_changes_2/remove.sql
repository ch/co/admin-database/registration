DROP VIEW registration.safety_training;

ALTER TABLE public.staff_category DROP COLUMN safety_training_url;
ALTER TABLE public.staff_category DROP COLUMN check_safety_training;

ALTER TABLE public.visitor_type_hid DROP COLUMN safety_training_url;
ALTER TABLE public.visitor_type_hid DROP COLUMN check_safety_training;

ALTER TABLE public.postgraduate_studentship_type DROP COLUMN safety_training_url;
ALTER TABLE public.postgraduate_studentship_type DROP COLUMN check_safety_training;

ALTER TABLE public.erasmus_type_hid DROP COLUMN safety_training_url;
ALTER TABLE public.erasmus_type_hid DROP COLUMN check_safety_training;
