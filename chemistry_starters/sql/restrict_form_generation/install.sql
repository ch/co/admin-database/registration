CREATE VIEW registration.crsids_allowed_to_generate_forms AS
SELECT
    person.crsid,
    form_types.form_type
FROM person
JOIN cache._latest_role_v12 lr ON person.id = lr.person_id
JOIN public._physical_status_v3 ps ON person.id = ps.person_id
CROSS JOIN (
    VALUES ('visitor'), ('staff'), ('research'), ('postgrad'), ('erasmus')
) form_types (form_type)
WHERE lr.status = 'Current'
AND
ps.status_id = 'Current'
AND
( lr.post_category = 'Assistant staff' OR lr.post_category = 'Academic-related staff' OR lr.post_category = 'Academic staff');

ALTER VIEW registration.crsids_allowed_to_generate_forms OWNER TO dev;

GRANT SELECT ON registration.crsids_allowed_to_generate_forms TO starters_registration;
