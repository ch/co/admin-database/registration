"""
Tests for functionality relating to the department safety handbook
"""
from unittest.mock import MagicMock, patch

import psycopg2

from .. import app, utils


class test_handbook(object):
    @classmethod
    def setUpClass(self):
        app.config["TEST_CRSID"] = "fjc55"
        self.client = app.test_client()

    def setup(self):
        "Set the safety handbook filename to a file that exists in the repo"
        app.config["SAFETY_HANDBOOK_FILENAME"] = "safety-handbook-placeholder.pdf"

    def test_download_handbook_from_form_generation(self):
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock:
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            # user is a member of chemistry
            database.cursor.rowcount = 1
            rv = self.client.get("/forms/safety-handbook")
            assert rv.status_code == 200
            # user is not a member of chemistry
            database.cursor.rowcount = 0
            rv = self.client.get("/forms/safety-handbook")
            assert rv.status_code != 200

    def test_download_handbook_from_safety_checklist(self):
        token = utils.generate_token_for_form()
        rv = self.client.get("/register/safety-handbook", query_string={"token": token})
        assert rv.status_code == 200
        rv = self.client.get("/register/safety-handbook", query_string={"token": ""})
        assert rv.status_code != 200

    def test_check_for_handbook(self):
        assert utils.safety.is_safety_handbook_file_available() is True
        app.config["SAFETY_HANDBOOK_FILENAME"] = "nonexistent.pdf"
        assert utils.safety.is_safety_handbook_file_available() is False
