from wtforms import Form

from ..forms.widgets import StrippedStringField, trinary


def test_strippedstringfield():
    """
    Check that the custom StrippedStringField class actually strips strings
    """

    class TestForm(Form):
        test_field = StrippedStringField()

    def check_strippedstringfield(inp, out):
        my_form = TestForm(test_field=inp)
        assert my_form.data.get("test_field") == out

    for params in [
        ("    ", ""),
        ("foo", "foo"),
        (2, 2),
        (None, None),
        ("foo   ", "foo"),
    ]:
        yield check_strippedstringfield, *params


def test_trinary():
    """
    Check the trinary function does the right thing
    """
    assert trinary("False") is False
    assert trinary("True") is True
    assert trinary(None) is None
