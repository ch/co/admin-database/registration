from unittest.mock import patch

from flask import g

from .. import app
from ..utils.form_generation import get_form_type_default


def test_get_form_type_default():
    with patch(
        "chemistry_starters.database.form_types_for_person"
    ) as get_form_types, app.app_context():
        g.crsid = "fjc55"  # set the crsid used by get_form_types
        get_form_types.return_value = ["visitor", "research"]
        assert get_form_type_default() is None
        get_form_types.return_value = ["visitor"]
        assert get_form_type_default() == "visitor"
