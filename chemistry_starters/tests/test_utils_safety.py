"""
Tests for utils functions
"""

import datetime
from unittest.mock import patch

from chemistry_starters.utils.safety import (
    is_safety_checklist_signed,
    is_safety_training_signed,
)


def check_safety_induction_signed_off(sign_date, result):
    with patch("chemistry_starters.database.get_form_column") as get_date:
        get_date.return_value = sign_date
        assert is_safety_checklist_signed("foo") == result


def test_signed_off_checkers():
    """
    Make sure we have logic correct for date checking safety checklist signoffs

    Might seem like overkill, but actually had this wrong
    """
    future = datetime.date.today() + datetime.timedelta(days=1)
    past1 = datetime.date.today()
    past2 = datetime.date.today() - datetime.timedelta(days=1)
    for d in [(past1, True), (past2, True), (future, False), (None, False)]:
        yield check_safety_induction_signed_off, d[0], d[1]
        yield check_safety_training_signed_off, d[0], d[1]


def check_safety_training_signed_off(sign_date, result):
    with patch("chemistry_starters.database.get_form_column") as get_date:
        get_date.return_value = sign_date
        assert is_safety_training_signed("foo") == result


# def test_safety_training_signed_off():
#    """
#    Make sure we have logic correct for date checking safety training signoffs
#    """
#    future = datetime.date.today() + datetime.timedelta(days=1)
#    past1 = datetime.date.today()
#    past2 = datetime.date.today() - datetime.timedelta(days=1)
#    for d in [(past1,True),(past2,True),(None,False)]:
#        yield check_safety_training_signed_off, d[0], d[1]
