"""
Test we can render safety forms as PDF
"""

import io
import uuid
from unittest.mock import MagicMock, patch

import psycopg2
from flask import g

from .. import app, utils


def test_save_pdf():
    with patch(
        "chemaccmgmt.db.ChemDB", autospec=True
    ) as db_mock, app.app_context(), app.test_request_context():
        database = db_mock.return_value
        database.conn = MagicMock(spec=psycopg2.extensions.connection)
        database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
        # have to import this inside a request context
        from chemistry_starters.forms.safety_forms import SafetyChecklist

        form = SafetyChecklist()
        rv = utils.safety.save_rendered_safety_form(
            uuid.uuid4(), form, "Example Starter"
        )
        assert rv is True


def test_access_pdf_allow():
    with patch("chemistry_starters.database.get_role_members") as role_members, patch(
        "chemistry_starters.utils.safety.get_safety_checklist_signer_crsid"
    ) as signer, app.app_context(), app.test_request_context():
        role_members.return_value = ["aaa11", "bbb12"]
        signer.return_value = "fjc55"
        g.crsid = "fjc55"
        assert utils.safety.check_safety_checklist_access(uuid.uuid4())


def test_access_pdf_fail():
    with patch("chemistry_starters.database.get_role_members") as role_members, patch(
        "chemistry_starters.utils.safety.get_safety_checklist_signer_crsid"
    ) as signer, app.app_context(), app.test_request_context():
        role_members.return_value = ["aaa11", "bbb12"]
        signer.return_value = "ccc123"
        g.crsid = "fjc55"
        assert not utils.safety.check_safety_checklist_access(uuid.uuid4())


def test_load_pdf():
    client = app.test_client()
    with patch(
        "chemistry_starters.utils.safety.check_safety_checklist_access"
    ) as checker, patch(
        "chemistry_starters.utils.safety.get_rendered_safety_checklist"
    ) as pdf, app.app_context():
        checker.return_value = True
        pdf.return_value = io.BytesIO()
        client.get(
            "/forms/completed/safety?token="
            + utils.generate_token_for_form(salt="rendered_safety_checklist")
        )
