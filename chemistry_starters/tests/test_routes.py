"""
Tests for the various routes in the app
"""
import os
from unittest.mock import MagicMock, patch

import psycopg2
from nose.plugins.attrib import attr
from nose.tools import assert_equal, assert_not_equal

from .. import app, utils

non_existent_user = "fjc55"


class test_routes(object):
    @classmethod
    def setUpClass(self):
        self.client = app.test_client()
        app.config["SECRET_KEY"] = os.urandom(
            20
        )  # not sure why I have to do this, but WTForms CSRF stuff breaks without it
        self.crsid_for_nonexistent_user = non_existent_user

    def set_user(self, username):
        app.config["TEST_CRSID"] = username

    def check_view(self, path, user="None", code=None, follow_redirects=False):
        if user:
            self.set_user(user)
        rv = self.client.get(path, follow_redirects=follow_redirects)
        if code:
            assert_equal(rv.status_code, code)
        else:
            assert_not_equal(rv.status_code, 404)

    def test_forbidden_paths(self):
        """Check URLs which should return 503 do so"""
        for path in ["/forbidden", "/unavailable"]:
            yield self.check_view, path, "anyone", 503

    def test_register(self):
        """Check the main register and safety views exist"""
        for path in [
            "/register",
            "/register/main",
            "/register/safety",
            "/register/safety_checklist",
            "/sign/safety",
            "/sign/safety_training",
        ]:
            yield self.check_view, path

    def test_example_safety_views(self):
        """Check the example safety views exists"""
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock:
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            for path in [
                "/forms/examples/safety",
                "/forms/examples/sign/safety",
                "/forms/examples/safety/with_student_training",
                "/forms/examples/safety/with_nonstudent_training",
                "/forms/examples/safety/without_training",
            ]:
                yield self.check_view, path

    def test_index_redirects(self):
        """The index page should redirect"""
        self.check_view("/", "anyone", 302)

    def test_error_paths_exist(self):
        """The various error endpoints"""
        for path in ["/expired", "/error", "/safety_training_not_found"]:
            yield self.check_view, path, "anyone", 500

    def test_form_not_found(self):
        self.check_view("/form_not_found", "anyone", 404)

    def test_all_static_paths(self):
        """
        Test routes for static content
        """
        for path in [
            "/jquery/jquery.min.js",
            "/jquery-ui/jquery-ui.min.js",
            "/static/css/style.css",
        ]:
            yield self.check_view, path, self.crsid_for_nonexistent_user, 200

    @attr("pl")
    def test_pl_static_path(self):
        """
        Test routes for pl content
        """
        for path in ["/pl/index.html"]:
            yield self.check_view, path, self.crsid_for_nonexistent_user, 200

    def setup_db_mock_for_user_check(self, db_mock, hits):
        """
        Configure a database mock to return the number of hits for a username
        found in the database
        """
        database = db_mock.return_value
        database.conn = MagicMock(spec=psycopg2.extensions.connection)
        database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
        database.cursor.rowcount = hits

    def test_access_controlled_views(self):
        """Check these views have some access control"""
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock:
            self.setup_db_mock_for_user_check(db_mock, 0)
            for path in [
                "/forms/new",
                "/forms/bulk",
                "/sign/safety_checklist?token="
                + utils.generate_token_for_form(salt="safety_checklist"),
                "/sign/safety_training",
            ]:
                yield self.check_view, path, self.crsid_for_nonexistent_user, 302
            self.setup_db_mock_for_user_check(db_mock, 1)
            for path in ["/forms/new", "/forms/bulk"]:
                yield self.check_view, path, self.crsid_for_nonexistent_user, 200

    def test_db_group_only_views(self):
        """Only people in certain database groups can access these"""
        with patch("chemistry_starters.database.get_role_members") as get_group, patch(
            "chemistry_starters.utils.safety.get_forms_awaiting_training_signoff"
        ) as get_forms:
            paths = [
                "/tokens/",
                "/tokens/decode",
                "/tokens/extend",
                "/tokens/custom",
                "/sign/safety_training",
            ]
            get_group.return_value = []
            get_forms.return_value = []
            for path in paths:
                yield self.check_view, path, self.crsid_for_nonexistent_user, 302
            get_group.return_value = [self.crsid_for_nonexistent_user]
            for path in paths:
                yield self.check_view, path, self.crsid_for_nonexistent_user, 200

    def test_safety_redirects(self):
        # Redirects, to '/register/safety_checklist'
        self.check_view("/register/safety", code=302)
        # But if we don't have a token, we finally end up at 'form not found'
        self.check_view("/register/safety", code=404, follow_redirects=True)
        # If we do have a token, but it's not known to the database (because I just created it)
        # then we end up at '/register/main'
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock, patch(
            "chemistry_starters.utils.person_role_details.load_main_form"
        ) as m:
            self.setup_db_mock_for_user_check(db_mock, 0)
            m.return_value = None
            rv = self.client.get(
                "/register/safety?token=" + utils.generate_token_for_form(),
                follow_redirects=True,
            )
            assert rv.status_code == 200
            assert (
                b"Welcome to the Yusuf Hamied Department of Chemistry online registration system."
                in rv.data
            )
