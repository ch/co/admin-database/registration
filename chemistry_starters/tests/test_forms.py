import os
from unittest.mock import MagicMock, patch

import psycopg2

from .. import app


class test_forms(object):
    @classmethod
    def setUpClass(self):
        self.client = app.test_client()
        app.config["SECRET_KEY"] = os.urandom(20)

    def test_registration_form_types(self):
        with patch(
            "chemaccmgmt.db.ChemDB", autospec=True
        ) as db_mock, app.test_request_context():
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            # Fake some hid lists in the style the database returns them
            database.cursor.fetchall.return_value = [[1, "A"], [2, "B"]]
            # The import here is the test - should not throw exceptions
            from .. import forms  # noqa F401
            from ..forms import person_role_data_forms  # noqa F401
            from ..forms import safety_forms  # noqa F401
