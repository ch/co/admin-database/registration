from ..auth import crsid_from_username


def test_crsid_from_username():
    crsid = "fjc55"
    raven_oauth2_good = "fjc55@cam.ac.uk"
    ucam_webauth_good = "fjc55"
    raven_oauth2_bad = "fjc55@example.com"

    assert crsid_from_username(raven_oauth2_good) == crsid
    assert crsid_from_username(ucam_webauth_good) == crsid
    assert crsid_from_username(raven_oauth2_bad) == "Nobody"
