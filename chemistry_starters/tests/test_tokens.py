import datetime
import os
from unittest.mock import MagicMock, patch

import psycopg2
from flask import request, url_for
from nose.tools import assert_equal

from .. import app, utils
from .utils import assert_flask_client_redirect


class test_tokens(object):
    @classmethod
    def setUpClass(self):
        self.client = app.test_client()
        app.config["SECRET_KEY"] = os.urandom(20)

    def test_decorator_good_token(self):
        token = utils.generate_token_for_form()
        with app.test_request_context("/register/main?token=" + token):

            @utils.check_token()
            def func(r):
                a = MagicMock()
                a(r)
                return a

            b = func(request)
            # b should be the MagicMock() we created inside func, and it should have been called
            assert b.called

    def test_decorator_bad_token(self):
        with app.test_request_context("/register/main?token="):

            @utils.check_token()
            def func(r):
                a = MagicMock()
                a(r)
                return a

            # The decorator should NOT have called function a, and b should be a response object
            # redirecting to form not found
            rv = func(request)
            assert rv.status_code == 302
            assert_equal(rv.location, url_for("error_pages.form_not_found"))

    def test_decorator_timed_out(self):
        token = utils.generate_token_for_form()
        with app.test_request_context("/register/main?token=" + token), patch(
            "chemistry_starters.utils.URLSafeTimedSerializer", autospec=True
        ) as serializer:
            instance = serializer.return_value
            instance.loads.side_effect = utils.SignatureExpired("")

            @utils.check_token()
            def func(r):
                a = MagicMock(r)
                a(r)
                return a

            # The decorator should NOT have called function a, and b should be a response object
            # redirecting to form expired
            rv = func(request)
            assert rv.status_code == 302
            print(rv.data)
            assert_equal(rv.location, url_for("error_pages.expired"))

    def test_nav_token_checks(self):
        """Token checking on navigation endpoint"""
        with patch("chemistry_starters.database.get_form_column"), patch(
            "chemistry_starters.database.email_for_current_person_id"
        ), patch(
            "chemistry_starters.database.get_safety_training_html_for_uuid"
        ), patch(
            "chemistry_starters.database.get_form_column"
        ), patch(
            "chemistry_starters.utils.person_role_details.is_form_submitted"
        ), patch(
            "chemistry_starters.utils.safety.is_form_submitted"
        ), patch(
            "chemistry_starters.database.name_for_person"
        ), patch(
            "chemistry_starters.utils.safety.is_safety_checklist_signed"
        ), patch(
            "chemistry_starters.database.is_safety_training_signoff_needed"
        ), patch(
            "chemistry_starters.utils.safety.is_safety_training_signed"
        ), patch(
            "chemistry_starters.utils.control.is_registration_complete"
        ):
            # valid token
            token = utils.generate_token_for_form()
            rv = self.client.get("/register", query_string={"token": token})
            assert_equal(rv.status_code, 200)
            # invalid token
            rv = self.client.get("/register", query_string={"token": "foo"})
            assert_equal(rv.status_code, 302)

    def test_valid_token_main(self):
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock, patch(
            "chemistry_starters.utils.person_role_details.save_main_form"
        ):
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            token = utils.generate_token_for_form()
            rv = self.client.get("/register/main", query_string={"token": token})
            # if the token was valid we should not be redirected
            assert_equal(rv.status_code, 200)
            assert b"Submit this form and return to task list" in rv.data

    def test_invalid_token_main(self):
        with app.app_context(), app.test_request_context():
            rv = self.client.get("/register/main", query_string={"token": "spong"})
            # if the token was invalid we should be redirected
            assert_equal(rv.status_code, 302)
            assert_flask_client_redirect(
                rv.location, url_for("error_pages.form_not_found")
            )

    def test_valid_token_safety(self):
        """
        Check the token checking is set up on the safety checklist endpoint
        """
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock, patch(
            "chemistry_starters.utils.control.is_registration_complete",
            autospec=True,
            return_value=True,
        ), app.test_request_context():
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            database.cursor.rowcount = 1
            database.cursor.fetchone = MagicMock()
            database.cursor.fetchone.side_effect = [
                [
                    None,
                    True,
                    True,
                ],  # utils.safety.load_safety_form in safety_checklist_form
                ["Foo Bar"],  # database.name_for_uuid in safety_checklist_form
                [1],  # utils.safety.get_safety_induction_signer in navigation_index
                [
                    '<a href="https://safety.training.url">https://safety.training.url</a>'
                ],  # database.get_safety_training_html_for_uuid
                ["pg-1"],  # get the post category
                [
                    True
                ],  # utils.person_role_details.is_form_submitted in navigation_index
                [
                    datetime.date.today()
                ],  # utils.safety.is_form_submitted in navigation_index
                ["Safety Checker"],  # database.name_for_person in navigation_index
                # utils.safety.is_safety_checklist_signed
                [datetime.date.today()],  # database.is_safety_training_signoff_needed
                [True],  # utils.safety.is_safety_training_signed
                [datetime.date.today()],  # utils.control.is_registration_complete
                # now into control.is_registration_complete
                [True],
                [True],
                [datetime.date.today()],
                [True],
                [datetime.date.today()],
            ]
            token = utils.generate_token_for_form()
            rv = self.client.get(
                "/register/safety_checklist",
                follow_redirects=True,
                query_string={"token": token},
            )
            print(rv.data)
            assert (
                b"""<p>You have already completed these tasks:</p>
  <ul>
    
    <li>Personal details form</li>
    
    
    <li>Safety checklist</li>
    
  </ul>"""  # noqa: W293
                in rv.data
            )
            assert b"Safety Checker" in rv.data

    def test_invalid_token_safety(self):
        with app.app_context(), app.test_request_context():
            rv = self.client.get(
                "/register/safety_checklist", query_string={"token": "spong"}
            )
            # if the token was invalid we should be redirected to form not found
            assert_equal(rv.status_code, 302)
            assert_flask_client_redirect(
                rv.location, url_for("error_pages.form_not_found")
            )

    def test_valid_token_safety_signoff(self):
        """
        Check the token checking is set up on the safety signoff form endpoint
        """
        with patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock, patch(
            "chemistry_starters.utils.control.is_registration_complete",
            autospec=True,
            return_value=True,
        ), app.test_request_context("/sign/safety"):
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            database.cursor.rowcount = 1
            database.cursor.fetchone = MagicMock()
            database.cursor.fetchone.side_effect = [
                ["fjc55"],  # get_safety_checklist_signer_crsid
                [datetime.date.today()],  # is_safety_checklist_signed
                ["Safety Checkee"],  # name_for_uuid
                ["Safety Checker"],  # name_for_person
            ]
            token = utils.generate_token_for_form(salt="safety_checklist")
            app.config["TEST_CRSID"] = "fjc55"
            rv = self.client.get(
                "/sign/safety_checklist", query_string={"token": token}
            )
            # if the token was valid we should not be redirected
            print(rv.data)
            assert_equal(rv.status_code, 200)
            assert b"The safety signoff for Safety Checkee has been recorded" in rv.data

    def test_invalid_token_safety_signoff(self):
        """
        Check the token checking is set up on the safety signoff form endpoint
        """
        with patch(
            "chemaccmgmt.db.ChemDB", autospec=True
        ) as db_mock, app.test_request_context("/sign/safety_checklist"):
            database = db_mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            database.cursor.rowcount = 1
            database.cursor.fetchone = MagicMock()
            database.cursor.fetchone.side_effect = [
                ["None"],
                [datetime.date.today()],
                ["Safety Checkee"],
                ["Safety Checker"],
            ]
            app.config["TEST_CRSID"] = "fjc55"
            rv = self.client.get(
                "/sign/safety_checklist", query_string={"token": "garbage"}
            )
            # if the token was invalid we should be redirected
            print(rv.data)
            assert_equal(rv.status_code, 302)
            assert_flask_client_redirect(
                rv.location, url_for("error_pages.form_not_found")
            )
