"""
Test the functionality which makes the application die in production if
a non-default secret key wasn't set
"""
from .. import app


def test_secret_key_check():
    client = app.test_client()
    from .. import default_settings

    app.debug = False
    app.config["SECRET_KEY"] = default_settings.SECRET_KEY
    rv = client.get("/", follow_redirects=True)
    assert b"The application's secret key is not set." in rv.data
    app.config["SECRET_KEY"] = ""
    rv = client.get("/", follow_redirects=True)
    assert b"The application's secret key is not set." not in rv.data
