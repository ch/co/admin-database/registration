from unittest.mock import MagicMock, patch

import psycopg2

from .. import app
from .. import database as database_module


def test_email_for_current_person_id():
    with patch(
        "chemaccmgmt.db.ChemDB", autospec=True
    ) as db_class_mock, app.test_request_context():
        db_instance_mock = db_class_mock.return_value
        db_instance_mock.conn = MagicMock(spec=psycopg2.extensions.connection)
        db_instance_mock.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
        db_instance_mock.cursor.rowcount = 1

        db_instance_mock.cursor.fetchone = MagicMock(
            return_value=[
                "daffy@example.com,fred@example.com",
            ]
        )
        mail = database_module.email_for_current_person_id(1)
        assert mail == ["daffy@example.com", "fred@example.com"]
        db_instance_mock.cursor.fetchone = MagicMock(
            return_value=[
                "daffy@example.com",
            ]
        )
        mail = database_module.email_for_current_person_id(1)
        assert mail == ["daffy@example.com"]
