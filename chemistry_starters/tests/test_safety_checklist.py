"""
Tests for the safety checklist view
"""
import io
import os
from unittest.mock import MagicMock, patch

import psycopg2
from flask import url_for

from .. import app, utils
from .utils import assert_flask_client_redirect

non_existent_user = "fjc55"


class test_safety_checklist(object):
    @classmethod
    def setUpClass(self):
        self.client = app.test_client()
        app.config["SECRET_KEY"] = os.urandom(
            20
        )  # not sure why I have to do this, but WTForms CSRF stuff breaks without it
        self.crsid_for_nonexistent_user = non_existent_user

    def test_safety_checklist_redirects(self):
        """
        Check that the safety checklist route redirects where appropriate
        """
        with app.test_request_context("/register/safety_checklist"), patch(
            "chemistry_starters.utils.safety.load_safety_form"
        ) as load_form, patch("chemistry_starters.database.name_for_uuid"):
            token = utils.generate_token_for_form()
            # both safety and main form complete, should redirect to main navigation
            load_form.return_value = [{}, True, True]
            rv = self.client.get(
                "/register/safety_checklist", query_string={"token": token}
            )
            assert rv.status_code == 302
            assert_flask_client_redirect(
                rv.location, url_for("starter_navigation.navigation_index", token=token)
            )
            # safety and main form not complete, should direct to main form
            load_form.return_value = [{}, False, False]
            rv = self.client.get(
                "/register/safety_checklist", query_string={"token": token}
            )
            assert rv.status_code == 302
            assert_flask_client_redirect(
                rv.location,
                url_for(
                    "person_role_details.person_role_details_form",
                    token=token,
                ),
            )

    def test_safety_checklist_loads(self):
        with patch("chemistry_starters.database.get_hids") as get_hids, patch(
            "chemistry_starters.utils.safety.load_safety_form"
        ) as load_form, patch("chemistry_starters.database.name_for_uuid"), patch(
            "chemistry_starters.database.get_safety_training_html_for_uuid"
        ) as training_html:
            token = utils.generate_token_for_form()
            # safety not complete, main form complete
            load_form.return_value = [{}, False, True]
            get_hids.return_value = [[1, "Value1"], [2, "Value2"]]
            training_html.return_value = None
            rv = self.client.get(
                "/register/safety_checklist", query_string={"token": token}
            )
            assert rv.status_code == 200

    def test_get_completed_safety_form(self):
        def setup_db_mock(mock, rowcount):
            database = mock.return_value
            database.conn = MagicMock(spec=psycopg2.extensions.connection)
            database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
            # check_acl looks at the value of cursor.rowcount
            database.cursor.rowcount = rowcount

        with app.test_request_context("/forms/completed/safety"), patch(
            "chemistry_starters.utils.safety.get_rendered_safety_checklist"
        ), patch("chemaccmgmt.db.ChemDB", autospec=True) as db_mock, patch(
            "chemistry_starters.utils.safety.check_safety_checklist_access"
        ) as access_allowed, patch(
            "chemistry_starters.utils.safety.get_rendered_safety_checklist"
        ) as get_file:

            # some data to return, representing the safety checklist pdf
            filedata = io.BytesIO()
            get_file.return_value = filedata

            # Check that if all the access control is satisfied we download the file.

            # the token is valid
            token = utils.generate_token_for_form(salt="rendered_safety_checklist")
            # user is staff member
            setup_db_mock(db_mock, 1)
            # user is allowed access to the requested form
            access_allowed.return_value = True
            rv = self.client.get(
                "/forms/completed/safety", query_string={"token": token}
            )
            assert rv.status_code == 200

            # Check we get blocked if the user is not a staff member

            # token is valid
            token = utils.generate_token_for_form(salt="rendered_safety_checklist")
            # user is /not/ a staff member
            setup_db_mock(db_mock, 0)
            # user is allowed access to the requested form
            access_allowed.return_value = True
            # We should be redirected and eventally end up at an access denied page
            rv = self.client.get(
                "/forms/completed/safety", query_string={"token": token}
            )
            assert rv.status_code == 302
            assert_flask_client_redirect(rv.location, url_for("error_pages.forbidden"))

            # Check we get blocked if the user asks for a form they don't have access to

            # token is valid
            token = utils.generate_token_for_form(salt="rendered_safety_checklist")
            # user is a staff member
            setup_db_mock(db_mock, 1)
            # user is /not/ allowed access to the requested form
            access_allowed.return_value = False
            # We should be redirected and eventally end up at an access denied page
            rv = self.client.get(
                "/forms/completed/safety", query_string={"token": token}
            )
            assert rv.status_code == 302
            assert_flask_client_redirect(rv.location, url_for("error_pages.forbidden"))

            # Check we get a 'not found' if the token isn't valid

            # token is invalid
            token = utils.generate_token_for_form(salt="junk")
            # user is a staff member
            setup_db_mock(db_mock, 1)
            # user is allowed access to the requested form;
            # we should never get to the check for this for this case
            access_allowed.return_value = True
            # We should be redirected and eventally end up at a 'form not found' page
            rv = self.client.get(
                "/forms/completed/safety", query_string={"token": token}
            )
            assert rv.status_code == 302
            assert_flask_client_redirect(
                rv.location, url_for("error_pages.form_not_found")
            )
