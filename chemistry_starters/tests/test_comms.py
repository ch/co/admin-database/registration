from unittest.mock import MagicMock, patch

import psycopg2

from .. import app
from ..utils import comms


def test_mail_form_creator_on_progress():
    with app.test_request_context(), patch("smtplib.SMTP") as mock_smtplib, patch(
        "chemaccmgmt.db.ChemDB"
    ) as mock_database:
        smtplib_instance_mock = MagicMock()
        mock_smtplib.return_value = smtplib_instance_mock
        db_instance_mock = MagicMock()
        mock_database.return_value = db_instance_mock
        db_instance_mock.conn = MagicMock(spec=psycopg2.extensions.connection)
        db_instance_mock.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
        db_instance_mock.cursor.rowcount = 1
        fetchone_return_values = [
            [1],  # notify_person_id for uuid
            ["daffy@example.com"],  # email for current person id
            ["Fred Bloggs"],  # name for uuid
            [True],  # is personal form submitted
            [True],  # is safety form submitted
            [None],  # date safety checklist was signed
            [False],  # is safety training signoff needed
            [None],  # date traiining was signed
        ]
        db_instance_mock.cursor.fetchone = MagicMock(side_effect=fetchone_return_values)
        comms.mail_form_creator_on_progress("some uuid")
        assert smtplib_instance_mock.send_message.called
