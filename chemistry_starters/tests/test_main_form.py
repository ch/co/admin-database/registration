"""
Tests for the safety checklist view
"""
import os
from unittest.mock import patch

from flask import url_for

from .. import app, utils
from ..roles import roles
from .utils import assert_flask_client_redirect

non_existent_user = "fjc55"


class test_main_form(object):
    @classmethod
    def setUpClass(self):
        self.client = app.test_client()
        app.config["SECRET_KEY"] = os.urandom(
            20
        )  # not sure why I have to do this, but WTForms CSRF stuff breaks without it
        self.crsid_for_nonexistent_user = non_existent_user

    def test_main_form_redirects(self):
        """
        Check that the main form redirects where appropriate
        """
        with app.test_request_context("/register/main"), patch(
            "chemistry_starters.utils.person_role_details.load_main_form"
        ) as load_form:
            token = utils.generate_token_for_form()
            # Main form complete, return to nav index
            load_form.return_value = None
            rv = self.client.get("/register/main", query_string={"token": token})
            assert rv.status_code == 302
            assert_flask_client_redirect(
                rv.location, url_for("starter_navigation.navigation_index", token=token)
            )

    def check_main_form_loads(self, form_type):
        with app.test_request_context("/register/main"), patch(
            "chemistry_starters.utils.person_role_details.load_main_form"
        ) as load_form, patch(
            "chemistry_starters.utils.person_role_details.save_main_form"
        ), patch(
            "chemistry_starters.database.get_hids"
        ) as get_hids:
            # main form not complete
            load_form.return_value = {}
            # set up token for form_type
            token = utils.generate_token_for_form(form_type=form_type)
            get_hids.return_value = [[1, "Foo"], [2, "Bar"]]
            rv = self.client.get("/register/main", query_string={"token": token})
            assert rv.status_code == 200

    def test_forms_load(self):
        for form_type in list(roles.keys()):
            yield self.check_main_form_loads, form_type
