from urllib.parse import urlparse


def assert_flask_client_redirect(client_url, expected_url):
    """
    Check if a Flask test client was redirected to the URL we expect

    This is only needed because of a change in behaviour between Jammy and
    Noble where the test client in Jammy reports a URL with a hostname, and
    the one in Noble without, even though the code redirected to one without.
    """
    assert client_url.endswith(expected_url)
    print(urlparse(client_url).path)
    print(urlparse(expected_url).path)
    assert urlparse(client_url).path == urlparse(expected_url).path
