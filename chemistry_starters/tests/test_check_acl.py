from unittest.mock import MagicMock, patch

import psycopg2
from flask import g, request

from .. import app
from ..utils import check_acl


def setup_mock(mock, rows):
    database = mock.return_value
    database.conn = MagicMock(spec=psycopg2.extensions.connection)
    database.cursor = MagicMock(spec=psycopg2.extras.DictCursor)
    database.cursor.rowcount = rows


def test_unknown_user():
    with patch(
        "chemaccmgmt.db.ChemDB", autospec=True
    ) as db_mock, app.test_request_context("/forms/new"):
        setup_mock(db_mock, 0)
        g.crsid = "fjc55"

        @check_acl()
        def func(r):
            b = MagicMock()
            b(r)
            return b

        a = func(request)
        print(a)
        # because we set up the database mock with a 'miss' we should not execute func(r)
        # but instead be redirected, so we don't get a MagicMock back
        # assert a is a flask.wrappers.Response
        # assert it's a 302 redirect
        assert a.status_code == 302


def test_known_user():
    with patch(
        "chemaccmgmt.db.ChemDB", autospec=True
    ) as db_mock, app.test_request_context("/forms/new"):
        setup_mock(db_mock, 1)
        g.crsid = "fjc55"

        @check_acl()
        def func(r):
            b = MagicMock()
            b(r)
            return b

        a = func(request)
        print(a)
        # because we set up the database mock with a 'hit' we should execute func(r),
        # which returns us a MagicMock
        assert a.called
