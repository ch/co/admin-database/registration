"""
Control flow functions
"""
from chemistry_starters import database

from . import comms, person_role_details, safety


def is_registration_complete(uuid, role=None):
    """
    Returns boolean as to whether a registration is completed

    Once this is true, the registration goes to the admin team for processing.
    """
    return (
        person_role_details.is_form_submitted(uuid)
        and safety.is_form_submitted(uuid)
        and safety.is_safety_checklist_signed(uuid)
        and (
            (not database.is_safety_training_signoff_needed(uuid))
            or safety.is_safety_training_signed(uuid)
        )
    )


def send_registration_to_admin_team(uuid):
    """
    Inform all the necessary admin people to process this registration
    """
    comms.mail_form_to_hr(uuid)
    comms.mail_form_to_photography(uuid)
