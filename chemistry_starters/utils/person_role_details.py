"""
Helper functions for the personal and role details form
"""

import datetime

import psycopg2
import psycopg2.extras
import psycopg2.sql as sql

from chemistry_starters import app, database
from chemistry_starters.roles import roles
from chemistry_starters.schema.personal_details_form import PersonalDetailsForm
from chemistry_starters.sqlalchemy_db import Session


def save_main_form(uuid, role, webform):
    """
    Save the current state of the main registration form to the database
    """
    session = Session()
    form = session.query(PersonalDetailsForm).filter_by(uuid=uuid).first()
    if not form:
        session.add(PersonalDetailsForm(uuid=uuid))
        form = session.query(PersonalDetailsForm).filter_by(uuid=uuid).first()
    # now update the form fields going to the database
    # We need to filter out -1 from optional select fields, as these are to be set null
    for field in webform:
        if field.type.startswith("ChosenSelect") and field.data == -1:
            field.data = None
    # FIXME check for exception here
    webform.populate_obj(form)
    if role == "research":
        form.post_category_id = database.get_hids("researcher_type")[0][0]
    if role == "embedded_company_staff":
        form.post_category_id = database.get_hids("embedded_company_staff_type")[0][0]
    # The safety form is now included
    form.separate_safety_form = False
    session.commit()


def is_form_submitted(uuid):
    """
    Checks if this form has already been completed, returns boolean
    """
    # None from database.get_form_column counts as 'not submitted'
    # as the form may not have hit the database at all yet
    if database.get_form_column("submitted", uuid):
        return True
    else:
        return False


def load_main_form(uuid):
    """
    Return anything saved in the database for this uuid
    """
    check_for_token_sql = "SELECT * FROM {schema}.{table} WHERE uuid = %s"
    check_for_token_query = sql.SQL(check_for_token_sql).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    db = database.get_db()
    dict_cursor = db.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        dict_cursor.execute(check_for_token_query, [uuid])
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up uuid {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
        return {}
    if dict_cursor.rowcount == 1:
        row = dict_cursor.fetchone()
        if row["submitted"]:
            return None
        else:
            return row
    else:
        return {}


def registration_template_factory(**kwargs):
    """
    Factory for producing the appropriate registration form template arguments for a role

    The main thing this does is create a form object of the right type.
    Returns a dict of keyword arguments to pass to render_template()
    """
    from chemistry_starters.forms import person_role_data_forms

    template_kwargs = {}
    template_kwargs["deadline"] = datetime.date.today() + datetime.timedelta(
        days=kwargs["days_left"]
    )
    template_kwargs["dob_max_date"] = datetime.date.today() - datetime.timedelta(
        days=365 * 18
    )
    template_kwargs["form_type"] = roles[kwargs["role"]]["form_title"]
    _formclass = getattr(person_role_data_forms, roles[kwargs["role"]]["form_class"])
    registration_form = _formclass(**kwargs)
    template_kwargs["form"] = registration_form
    return template_kwargs
