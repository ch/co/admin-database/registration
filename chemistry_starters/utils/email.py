"""
This is a replacement for chemaccmgmt's send_email
"""
import smtplib
from email.message import EmailMessage

import chemaccmgmt.config as config


def send_email(
    to_addresses,
    from_address,
    subject,
    body,
    cc_addresses=None,
    bcc_addresses=None,
    attachments=None,
    envelope_from=None,
    quiet=True,
):
    """
    Sends an email, optionally with attachments.

    Attachments should be passed as a list of dictionaries with keys
    maintype, subtype, and one of data or filename. Optionally name
    can be set too. data should support read().
    Returns False if failed to send email, True otherwise.
    """
    worked = True
    # copy the list
    to_list = list(to_addresses)
    if not cc_addresses:
        cc_addresses = []
    if not bcc_addresses:
        bcc_addresses = []
    all_addresses = list(to_list)
    all_addresses.extend(cc_addresses)
    all_addresses.extend(bcc_addresses)
    if not envelope_from:
        envelope_from = from_address

    msg = EmailMessage()
    msg.set_content(body)
    if not attachments:
        attachments = {}

    msg["Subject"] = subject
    msg["From"] = from_address
    if len(to_list) != 0:
        msg["To"] = ",".join(to_list)
    if len(cc_addresses) != 0:
        msg["Cc"] = ",".join(cc_addresses)

    if attachments:
        for attachment in attachments:
            if "filename" in attachment:
                with open(attachment["filename"], "rb") as fp:
                    data = fp.read()
            else:
                data = attachment["data"].read()
            kwargs = {
                "maintype": attachment["maintype"],
                "subtype": attachment["subtype"],
            }
            if "name" in attachment:
                kwargs["filename"] = attachment["name"]
            msg.add_attachment(data, **kwargs)
    if not quiet:
        txt = "Sending mail to %s from %s body %s"
        print(txt % (",".join(all_addresses), envelope_from, msg.as_string()))
    s = smtplib.SMTP(config.get("Email", "smtp_server"))
    try:
        s.send_message(msg, envelope_from, all_addresses)
    except smtplib.SMTPRecipientsRefused:
        errormsg = "HELP: could not send mail to %s"
        print(errormsg % (",".join(all_addresses)))
        worked = False
    s.quit()
    return worked
