"""
Functions to communicate with the outside world
"""

import os

import jinja2
import psycopg2
import psycopg2.sql as sql
from flask import url_for

from chemistry_starters import app, database, utils
from chemistry_starters.schema.personal_details_form import PersonalDetailsForm
from chemistry_starters.sqlalchemy_db import Session
from chemistry_starters.utils.safety import is_safety_handbook_file_available

from . import email, person_role_details, safety


def mail_form_to_hr(uuid):
    """
    Send HR an email saying that the form with uuid is ready to be processed

    If we ever change where in the process this function gets called we also
    need to update the database view hotwire3."10_View/People/Registration"
    to match - the database view should show the forms that email has been sent
    about and (probably) not any others.
    """
    # build body of email
    db = database.get_db()
    query = sql.SQL(
        """
    SELECT * FROM {schema}.{view} WHERE uuid = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        view=sql.Identifier(app.config["COMPLETED_FORMS_VIEW"]),
    )
    try:
        db.cursor.execute(query, [uuid])
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to connect to database to look up row for uuid {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
        return False
    if db.cursor.rowcount == 1:
        templates = jinja2.Environment(
            loader=jinja2.PackageLoader("chemistry_starters", "templates")
        )
        hr_email_template = templates.get_template("emails/hr_email")
        email_body = hr_email_template.render(data=db.cursor.fetchone())
        worked = email.send_email(
            app.config["HR_EMAIL_ADDRESSES"],
            app.config["FROM_EMAIL_ADDRESS"],
            "Completed registration form",
            email_body,
            envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
        )
        return worked


def mail_form_to_photography(uuid):
    """
    Inform photography there's a photo to be processed
    """
    templates = jinja2.Environment(
        loader=jinja2.PackageLoader("chemistry_starters", "templates")
    )
    photo_template = templates.get_template("emails/photo_email")
    session = Session()
    form = session.query(PersonalDetailsForm).filter_by(uuid=uuid).first()
    template_kwargs = form.__dict__.copy()
    post_category = [
        x[1]
        for x in database.get_hids("post_category")
        if x[0] == form.post_category_id
    ][0]
    template_kwargs["post_category"] = post_category
    try:
        supervisor = [
            x[1]
            for x in database.get_hids("supervisor")
            if x[0] == form.department_host_id
        ][0]
        template_kwargs["supervisor"] = supervisor
    except IndexError:
        template_kwargs["supervisor"] = "Supervisor not found"
    email_body = photo_template.render(**template_kwargs)
    result = email.send_email(
        app.config["PHOTOGRAPHY_EMAIL_ADDRESSES"],
        app.config["FROM_EMAIL_ADDRESS"],
        "Department of Chemistry Registration form",
        email_body,
        envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
    )
    return result


def mail_form_creator_on_progress(uuid):
    """
    Email the person who created the form a progress message
    """
    notify_person_id = database.get_form_column("notify_person_id", uuid)
    if notify_person_id:
        emails = database.email_for_current_person_id(notify_person_id)
        if emails:
            starter_name = database.name_for_uuid(uuid)
            templates = jinja2.Environment(
                loader=jinja2.PackageLoader("chemistry_starters", "templates")
            )
            progress_template = templates.get_template("emails/progress")
            email_body = progress_template.render(
                starter_name=starter_name,
                personal_data_complete=person_role_details.is_form_submitted(uuid),
                safety_checklist_complete=safety.is_form_submitted(uuid),
                safety_induction_signed=safety.is_safety_checklist_signed(uuid),
                safety_training_signoff_needed=database.is_safety_training_signoff_needed(
                    uuid
                ),
                safety_training_signed=safety.is_safety_training_signed(uuid),
            )
            result = email.send_email(
                emails,
                app.config["FROM_EMAIL_ADDRESS"],
                f"Department of Chemistry Registration progress for {starter_name}",
                email_body,
                envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
            )
            return result


def email_starter(address, url):
    """
    Send an email to a new starter with their unique registration URL
    """
    attachments = None
    handbook_text = ""
    if is_safety_handbook_file_available():
        attachments = [
            {
                "filename": os.path.join(
                    app.root_path, "documents", app.config["SAFETY_HANDBOOK_FILENAME"]
                ),
                "name": "safety-handbook.pdf",
                "maintype": "application",
                "subtype": "pdf",
            }
        ]
        handbook_text = (
            "A copy of the Department Safety Handbook is attached to this email. "
            "You will need this to complete the registration safety checklist."
        )
    templates = jinja2.Environment(
        loader=jinja2.PackageLoader("chemistry_starters", "templates")
    )
    email_template = templates.get_template("emails/starter_email")
    email_body = email_template.render(
        url=url, lifetime=app.config["FORM_LIFETIME_DAYS"], extra_text=handbook_text
    )

    return email.send_email(
        [address],
        app.config["FROM_EMAIL_ADDRESS"],
        "Department of Chemistry Registration form",
        email_body,
        envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
        attachments=attachments,
    )


def email_incomplete_form_reminders(quiet=True):
    """
    Email reminders to people who haven't completed their forms

    Three reminders are sent. The first two extend the form.
    """
    # No point sending reminders if the form lifetime is set very low
    if app.config["FORM_LIFETIME_DAYS"] <= 1:
        return
    templates = jinja2.Environment(
        loader=jinja2.PackageLoader("chemistry_starters", "templates")
    )
    email_template = templates.get_template("emails/incomplete_registration_reminder")
    forms_to_remind = database.get_incomplete_form_reminders(
        [app.config["REMINDER_INTERVAL"] * repeat for repeat in range(1, 4)]
    )
    for db_row in forms_to_remind:
        form = dict(db_row)
        form["reminder_interval"] = app.config["REMINDER_INTERVAL"]
        token = utils.generate_token_for_form(
            form_type=form["form_role"],
            form_uuid=form["uuid"],
            salt="form",
            reminder_token=True,
        )
        form["url"] = url_for(
            "starter_navigation.navigation_index", _external=True, token=token
        )
        email_body = email_template.render(**form)
        addresses = [form["starter_email"]]
        if form["admin_email"]:
            addresses.append(form["admin_email"])
        email.send_email(
            addresses,
            app.config["FROM_EMAIL_ADDRESS"],
            "Department of Chemistry Registration Reminder",
            email_body,
            envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
            quiet=quiet,
        )
