"""
Helper functions
"""
import time
import uuid
from functools import wraps

import jinja2
import psycopg2
import psycopg2.extras
import psycopg2.sql as sql
from flask import g, redirect, request, url_for
from itsdangerous import (
    BadSignature,
    BadTimeSignature,
    SignatureExpired,
    TimestampSigner,
    URLSafeTimedSerializer,
)

from chemistry_starters import app, database

from . import person_role_details  # noqa F401
from . import safety
from .email import send_email


class EpochOffsetSigner(TimestampSigner):
    """
    A Signer class that behaves like itsdangerous.TimestampSigner from version 0.24

    The behaviour of itsdangerous.TimestampSigner changed significantly in version 1.1.0
    such that an application running 1.1.0 fails to validate timed tokens created by 0.24.
    See https://github.com/pallets/itsdangerous/issues/120
    This provides a version that behaves like 0.24.
    """

    # 1/1/2011 in UTC
    EPOCH = 1293840000

    def get_timestamp(self):
        return int(time.time() - self.EPOCH)


class ReminderSigner(EpochOffsetSigner):
    """
    A signer class that shifts the creation timestamp into the past

    Used for generating form URLs in reminders without extending the form life excessively
    """

    def get_timestamp(self):
        return super().get_timestamp() - (app.config["REMINDER_INTERVAL"] * 86400)


def db_groups_only(*groupnames):
    def decorate_db_groups_only(f):
        @wraps(f)
        def wrapper_db_groups_only(*args, **kwargs):
            """
            Allow members of specified database group only
            """
            crsid = g.crsid
            if crsid not in database.get_role_members(*groupnames):
                return redirect(url_for("error_pages.forbidden", _external=False))
            else:
                return f(*args, **kwargs)

        return wrapper_db_groups_only

    return decorate_db_groups_only


def check_acl(acl_view=app.config["CURRENT_CRSIDS_VIEW"]):
    def decorate_check_acl(f):
        @wraps(f)
        def wrapper_check_acl(*args, **kwargs):
            """
            Check if crsid is in the ACL view
            """
            crsid = g.crsid
            try:
                d = database.get_db()
            except database.DatabaseNotAvailable:
                return redirect(url_for("error_pages.unavailable", _external=False))
            check_sql = sql.SQL(
                "select crsid from {schema}.{acl_view} where crsid = %s"
            )
            try:
                d.cursor.execute(
                    check_sql.format(
                        schema=sql.Identifier(app.config["DB_SCHEMA"]),
                        acl_view=sql.Identifier(acl_view),
                    ),
                    [crsid],
                )
                database_hits = d.cursor.rowcount
            except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
                app.logger.error(
                    "Failed to connect to database to look up {}, error was {}".format(
                        crsid, str(e)
                    )
                )
                database_hits = 0
                d.conn.rollback()
            if database_hits == 0:
                return redirect(url_for("error_pages.forbidden", _external=False))
            else:
                return f(*args, **kwargs)

        return wrapper_check_acl

    return decorate_check_acl


def generate_token_for_form(
    form_type="visitor", form_uuid=None, salt="form", reminder_token=False
):
    """
    Returns a token representing a time-limited form of form_type

    This generates a token which combines a UUID and some other data about
    a form instance encoded as a string that's safe to use in URLs, but cannot be
    decoded to recover the UUID and data unless you have the app's secret key.
    Requests to register have to supply a valid token, but don't need to come
    from inside Chemistry or be authenticated in any other way.
    """
    if reminder_token:
        token_signer = ReminderSigner
    else:
        token_signer = EpochOffsetSigner
    s = URLSafeTimedSerializer(app.config["SECRET_KEY"], salt=salt, signer=token_signer)
    if form_uuid is None:
        # uuid4 generates a 'random' uuid
        u = uuid.uuid4()
    else:
        u = uuid.UUID(form_uuid)
    token = (form_type, u.hex)
    result = s.dumps(token)
    return result


def check_token(max_age=60 * 60 * 24 * app.config["FORM_LIFETIME_DAYS"], salt="form"):
    def decorate_check_token(f):
        @wraps(f)
        def wrapper_check_token(*args, **kwargs):
            """
            Check if token was issued by this app and hasn't timed out
            """
            token = request.args.get("token")
            if token is None:
                return redirect(url_for("error_pages.form_not_found", _external=False))
            s = URLSafeTimedSerializer(
                app.config["SECRET_KEY"], salt=salt, signer=EpochOffsetSigner
            )
            try:
                s.loads(token, max_age=max_age, salt=salt)
            except SignatureExpired:
                return redirect(url_for("error_pages.expired", _external=False))
            except (BadSignature, BadTimeSignature):
                return redirect(url_for("error_pages.form_not_found", _external=False))
            return f(*args, **kwargs)

        return wrapper_check_token

    return decorate_check_token


def decode_token(salt="form", token=None):
    """
    Return a decoded token
    """
    if not token:
        token = request.args.get("token")
    s = URLSafeTimedSerializer(
        app.config["SECRET_KEY"], salt=salt, signer=EpochOffsetSigner
    )
    role, uuid = s.loads(token, salt=salt)
    return uuid, role


def get_token_days_remaining(salt="form", token=None):
    """
    Return the remaining valid time of the token in days or None if not valid
    """
    if not token:
        return None
    try:
        decode_token(salt, token)
    except BadSignature:
        return None
    s = URLSafeTimedSerializer(
        app.config["SECRET_KEY"], salt=salt, signer=EpochOffsetSigner
    )
    for days in range(app.config["FORM_LIFETIME_DAYS"], -1, -1):
        try:
            s.loads(token, max_age=(days * 24 * 60 * 60), salt=salt)
        except SignatureExpired:
            break
    return app.config["FORM_LIFETIME_DAYS"] - days


def mail_safety_induction_signoff(uuid):
    """
    Mail the person who signs off the safety induction
    """
    signer_id = database.get_form_column("safety_induction_person_signing_off_id", uuid)
    signer_emails = database.email_for_current_person_id(signer_id)
    if signer_emails:
        templates = jinja2.Environment(
            loader=jinja2.PackageLoader("chemistry_starters", "templates")
        )
        email_template = templates.get_template("emails/safety_signoff_email")
        starter_name = database.name_for_uuid(uuid)
        email_body = email_template.render(
            url=url_for(
                "safety_checklist.safety_checklist_signoff",
                _external=True,
                token=generate_token_for_form(
                    form_uuid=uuid,
                    salt="safety_checklist",
                    form_type="safety_checklist",
                ),
            ),
            name=starter_name,
        )
        memory_file = safety.get_rendered_safety_checklist(uuid)
        worked = send_email(
            signer_emails,
            app.config["FROM_EMAIL_ADDRESS"],
            "Department of Chemistry Registration Safety Checklist",
            email_body,
            envelope_from=app.config["ENVELOPE_FROM_ADDRESS"],
            attachments=[
                {
                    "data": memory_file,
                    "maintype": "application",
                    "subtype": "pdf",
                    "name": "safety_checklist.pdf",
                }
            ],
        )
        return worked
