"""
Helper functions for the form generation views
"""
from flask import g

from chemistry_starters import database
from chemistry_starters.forms import form_generation as forms

from . import person_role_details


def save_initial_form(uuid, role, notify_person, email, post_category_id=None):
    """
    Save uuid, creator, and starter's email for a new form to database.

    This is so that we can set up reminder jobs to nag people to
    fill in their forms.
    """
    form = forms.RecordFormInDatabaseForm()
    # fill form here, don't need to validate because email came from
    # an already-validated form
    form.email.data = email
    form.form_role.data = role
    form.post_category_id.data = post_category_id
    person_id = None
    if notify_person is not None:
        person_id = database.id_for_crsid(notify_person)
    if person_id is not None:
        form.notify_person_id.data = person_id
    else:
        del form.notify_person_id
    person_role_details.save_main_form(uuid, role, form)


def is_form_imported(uuid):
    """
    Check if a form has been imported to the main database
    """
    import_date = database.get_form_column("_imported_on", uuid)
    return import_date is not None


def get_form_type_default():
    """
    Get the default form type

    Usually there is no default type, but if the person only has one form type
    available, return that
    """
    form_types = database.form_types_for_person(g.crsid)
    if len(form_types) == 1:
        return form_types[0]
    else:
        return None
