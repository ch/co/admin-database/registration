"""
Safety specific utility functions
"""

import datetime
import io
import os

import pdfkit
import psycopg2
import psycopg2.sql as sql
from flask import g, render_template, request, url_for

from chemistry_starters import app, database


def is_safety_checklist_signed(uuid):
    """
    Check if form was signed, returns True or False
    """
    signed_date = database.get_form_column("safety_induction_signed_off_date", uuid)
    if signed_date is not None:
        return signed_date <= datetime.date.today()
    return False


def save_safety_form_state(uuid, form):
    """
    Record the current state of a safety form in the database

    This saves the safety questions into a single database field as json. We
    don't save individual fields to database columns because the safety
    questions and answers vary year to year, and don't get any further
    processing in the database - we just need to store the answers. Doing it as
    json avoids having to change the database schema all the time.

    It also checks whether the registration is for someone whose safety
    training must be checked, and sets the appropriate column in the row for
    the registration to True
    """

    questions_and_answers = {}
    for field in form:
        # Record all the safety questions, except safety_submitted which goes in its
        # own db column and is only set to true when the form validates
        if field.name.startswith("safety") and field.name != "safety_submitted":
            questions_and_answers[field.name] = field.data
    # The signoff person id needs extra processing. It is turned into a
    # friendly person name in the json for readability. Also we use -1 as a
    # placeholder value in the form and we must check for that and not send it
    # to the database, as it's not a valid person id.
    if "safety_induction_person_signing_off_id" in form:
        questions_and_answers[
            "safety_induction_person_signing_off"
        ] = database.name_for_person(
            form.safety_induction_person_signing_off_id.data, identifier_type="id"
        )
        if questions_and_answers["safety_induction_person_signing_off"] is not None:
            database.save_form_column(
                "safety_induction_person_signing_off_id",
                form.safety_induction_person_signing_off_id.data,
                uuid,
            )
    database.save_form_column(
        "safety_answers", psycopg2.extras.Json(questions_and_answers), uuid
    )
    database.save_form_column(
        "safety_training_needs_signoff",
        database.is_safety_training_signoff_needed(uuid),
        uuid,
    )


def save_rendered_safety_form(uuid, form, starter_name):
    """
    Save the final state of the safety checklist as PDF and HTML and mark as done

    Returns False if it failed, True if it succeeded
    """
    safety_training_html = database.get_safety_training_html_for_uuid(uuid)
    rendered_html = render_template(
        "safety/safety_form.html",
        form=form,
        enable_buttons=False,
        starter_name=starter_name,
        safety_training_html=safety_training_html,
    )

    database.save_form_column("safety_form_html", rendered_html, uuid)
    try:
        pdf = pdfkit.from_string(
            rendered_html,
            False,
            options={"enable-local-file-access": None, "disable-javascript": None},
        )
    except IOError as e:
        error_str = "Could not render safety checklist HTML as PDF for {}, error was {}, url was {}"
        app.logger.error(
            error_str.format(
                uuid,
                str(e),
                url_for(
                    "safety_checklist.safety_checklist_form",
                    _external=True,
                    token=request.args.get("token"),
                ),
            )
        )
        pdf = None
        return False
    if pdf is not None:
        database.save_form_column("safety_form_pdf", pdf, uuid)
        database.save_form_column("safety_submitted", True, uuid)
    return True


def load_safety_form(uuid):
    """

    Return tuple of form data, whether form submitted, whether main form submitted

    See also save_safety_form_state()

    Because of the way json conversion works, if the safety checklist data
    we're fetching is null we get None rather than an empty dict back from the
    database.
    """
    load_sql = sql.SQL(
        """
    SELECT safety_answers,safety_submitted,submitted as main_form_submitted
    FROM {schema}.{table} WHERE uuid = %s AND submitted = 't'
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    db = database.get_db()
    try:
        db.cursor.execute(load_sql, [uuid])
        if db.cursor.rowcount == 1:
            return db.cursor.fetchone()
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to load safety data from database for UUID {}, error was {}".format(
                uuid, str(e)
            )
        )
        db.conn.rollback()
    return {}, False, False


def is_form_submitted(uuid):
    """
    Checks if this form has already been completed, returns boolean
    """
    # None from database.get_form_column counts as 'not submitted'
    # as the form may not have hit the database at all yet
    if database.get_form_column("safety_submitted", uuid):
        return True
    else:
        return False


def check_safety_checklist_access(uuid):
    """
    Check if the calling user can access the safety form data for uuid, return True or False
    """
    # Normally these groups will access the form via the hoitwire interface; this ACL check
    # is just for exceptional circumstances
    allowed_list = database.get_role_members("hr", "dev", "safety_management")
    allowed_list.append(get_safety_checklist_signer_crsid(uuid))
    if g.crsid in allowed_list:
        return True
    return False


def get_rendered_safety_checklist(uuid):
    """
    Return in-memory file containing the rendered HTML for uuid's safety checklist
    """
    memory_file = io.BytesIO()
    memory_file.write(database.get_form_column("safety_form_pdf", uuid))
    memory_file.seek(0)
    return memory_file


def get_safety_checklist_signer_crsid(uuid):
    """
    Return the crsid of the person who is to sign off uuid's safety checklist
    """
    signer_sql = sql.SQL(
        """
    SELECT {people}.crsid FROM {schema}.{table}
    JOIN {schema}.{people} ON {table}.safety_induction_person_signing_off_id = {people}.id
    WHERE uuid = %s
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
        people=sql.Identifier(app.config["CURRENT_CRSIDS_VIEW"]),
    )
    db = database.get_db()
    try:
        db.cursor.execute(signer_sql, [uuid])
    except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
        app.logger.error(
            "Failed to look up signer for uuid {}, error was {}".format(uuid, e)
        )
        db.conn.rollback()
        return None
    if db.cursor.rowcount == 1:
        return db.cursor.fetchone()[0]
    return None


def get_forms_awaiting_training_signoff():
    """
    Returns a list of forms awaiting training signoff
    """
    form_sql = sql.SQL(
        """
    SELECT uuid
    FROM {schema}.{table}
    WHERE safety_training_needs_signoff AND ( safety_training_signed_off_date IS NULL )
    ORDER BY surname, first_names, crsid
    """
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
    )
    db = database.get_db()
    try:
        db.cursor.execute(form_sql)
    except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
        app.logger.error(
            "Failed to look up forms needing training signoff, error was {}".format(e)
        )
        db.conn.rollback()
        return []
    return [x[0] for x in db.cursor.fetchall()]


def set_safety_training_signed(uuid, crsid):
    """
    Marks uuid as having had their safety training checked
    """
    update_sql = sql.SQL(
        """UPDATE {schema}.{table}
           SET safety_training_person_signing_off_id =
                 (SELECT id FROM {schema}.{crsids} WHERE crsid = %s),
               safety_training_signed_off_date = current_date
           WHERE uuid = %s"""
    ).format(
        schema=sql.Identifier(app.config["DB_SCHEMA"]),
        table=sql.Identifier(app.config["FORM_TABLE"]),
        crsids=sql.Identifier(app.config["CURRENT_CRSIDS_VIEW"]),
    )
    db = database.get_db()
    try:
        db.cursor.execute(update_sql, [crsid, uuid])
    except (psycopg2.ProgrammingError, psycopg2.OperationalError) as e:
        app.logger.error(
            "Failed to update safety training for {} by {}, error was {}".format(
                uuid, crsid, str(e)
            )
        )
        db.conn.rollback()
        return False
    db.conn.commit()
    return True


def is_safety_training_signed(uuid):
    """
    Return boolean as to whether the training has been signed off

    Does not check whether training _needs_ signing off.
    """
    signoff_date = database.get_form_column("safety_training_signed_off_date", uuid)
    return signoff_date is not None and signoff_date <= datetime.date.today()


def is_safety_handbook_file_available():
    """
    Returns a boolean to say whether this app has a copy of the Safety Handbook

    The Department Safety handbook is to be kept secret, so we can't keep a copy
    with the code. When the app is installed the handbook PDF should be placed in the documents
    folder at the filename given by the SAFETY_HANDBOOK_FILENAME config parameter. This
    function is provided to allow the app to check that this has been done and warn if not.
    """
    return os.path.isfile(
        os.path.join(app.root_path, "documents", app.config["SAFETY_HANDBOOK_FILENAME"])
    )
