from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

from chemistry_starters import app

metadata = MetaData(schema=app.config["DB_SCHEMA"])
Base = declarative_base(metadata=metadata)
