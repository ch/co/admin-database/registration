from sqlalchemy import BigInteger, Boolean, Column, Date, String, Text
from sqlalchemy.types import ARRAY

from . import Base


class PersonalDetailsForm(Base):
    __tablename__ = "form"

    uuid = Column(String, primary_key=True)
    first_names = Column(Text)
    surname = Column(Text)
    known_as = Column(Text)
    title_id = Column(BigInteger)
    date_of_birth = Column(Date)
    nationality_id = Column(ARRAY(BigInteger))
    gender_id = Column(BigInteger)
    previously_registered = Column(Boolean)
    dept_room_id = Column(ARRAY(BigInteger))
    home_address = Column(Text)
    home_phone_number = Column(Text)
    mobile_number = Column(Text)
    email = Column(Text)
    crsid = Column(Text)
    emergency_contact_name_1 = Column(Text)
    emergency_contact_number_1 = Column(Text)
    emergency_contact_name_2 = Column(Text)
    emergency_contact_number_2 = Column(Text)
    separate_safety_form = Column(Boolean, default=False)

    start_date = Column(Date)
    intended_end_date = Column(Date)
    job_title = Column(Text)
    post_category_id = Column(BigInteger)
    department_host_id = Column(BigInteger)

    college_id = Column(BigInteger)

    contact_preference_id = Column(BigInteger)
    contact_preference_details = Column(Text)
    dept_telephone_id = Column(ARRAY(BigInteger))
    hide_from_website = Column(Boolean, default=False)

    previously_registered = Column(Boolean, default=False)
    already_has_university_card = Column(Boolean)
    submitted = Column(Boolean, default=False)
    form_role = Column(Text)
    notify_person_id = Column(BigInteger)
