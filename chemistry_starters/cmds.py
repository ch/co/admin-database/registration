import argparse
import sys

import jinja2
from flask import url_for

import chemistry_starters
from chemistry_starters.utils import send_email
from chemistry_starters.utils.comms import email_incomplete_form_reminders
from chemistry_starters.utils.safety import get_forms_awaiting_training_signoff


def check_for_registrations_waiting():
    """
    To check from the CLI whether safety training signoffs are waiting
    """
    with chemistry_starters.app.app_context():
        try:
            if get_forms_awaiting_training_signoff():
                print("Registrations are waiting")
                sys.exit(1)
            else:
                print("No registrations are waiting")
                sys.exit(0)
        except chemistry_starters.database.DatabaseNotAvailable:
            print("Cannot access database to check for registrations")
            sys.exit(9)


def email_incomplete_registrations():
    """
    To email people who haven't completed their registrations
    """
    p = argparse.ArgumentParser(
        description="Email people whose regsistrations have not been completed"
    )
    p.add_argument(
        "-q",
        action="store_true",
        dest="quiet",
        help="Run quietly, unless there is an error",
    )
    p.add_argument(
        "-d", action="store_true", dest="dryrun", help="Do not send any email"
    )
    p.add_argument(
        "-s",
        action="store",
        dest="server_name",
        required=True,
        help="Set the server name of the web application, for use in emails",
    )
    p.add_argument(
        "-r",
        action="store",
        dest="app_root",
        required=True,
        help="Set the base URL of the web application, for use in emails",
    )

    args = p.parse_args()

    chemistry_starters.app.config["SERVER_NAME"] = args.server_name
    chemistry_starters.app.config["APPLICATION_ROOT"] = args.app_root
    chemistry_starters.app.config["PREFERRED_URL_SCHEME"] = "https"

    with chemistry_starters.app.app_context():
        email_incomplete_form_reminders(quiet=args.quiet)


def email_waiting_registrations():
    """
    To email when safety training signoffs are needed
    """
    p = argparse.ArgumentParser(
        description="Check if safety training signoffs are needed and send email if they are"
    )
    p.add_argument(
        "-q",
        action="store_true",
        dest="quiet",
        help="Run quietly, unless there is an error",
    )
    p.add_argument(
        "-d", action="store_true", dest="dryrun", help="Do not send any email"
    )
    p.add_argument(
        "-t",
        action="store_true",
        dest="test",
        help="Test mode, don't check, just email",
    )
    p.add_argument(
        "-s",
        action="store",
        dest="server_name",
        required=True,
        help="Set the server name of the web application, for use in emails",
    )
    p.add_argument(
        "-r",
        action="store",
        dest="app_root",
        required=True,
        help="Set the base URL of the web application, for use in emails",
    )

    args = p.parse_args()

    chemistry_starters.app.config["SERVER_NAME"] = args.server_name
    chemistry_starters.app.config["APPLICATION_ROOT"] = args.app_root
    chemistry_starters.app.config["PREFERRED_URL_SCHEME"] = "https"

    with chemistry_starters.app.app_context():
        try:
            waiting = get_forms_awaiting_training_signoff()
        except chemistry_starters.database.DatabaseNotAvailable:
            print("Cannot access database to check for registrations")
            sys.exit(9)
        if waiting or args.test:
            templates = jinja2.Environment(
                loader=jinja2.PackageLoader("chemistry_starters", "templates")
            )
            safety_training_template = templates.get_template(
                "emails/safety_training_signoff"
            )
            email_body = safety_training_template.render(
                safety_signoff_url=url_for(
                    "safety_training.safety_training_signoff", _external=True
                )
            )
            if not args.dryrun:
                worked = send_email(
                    chemistry_starters.app.config["SAFETY_TRAINING_EMAIL_ADDRESSES"],
                    chemistry_starters.app.config["FROM_EMAIL_ADDRESS"],
                    "Safety training signoffs are waiting",
                    email_body,
                    envelope_from=chemistry_starters.app.config[
                        "ENVELOPE_FROM_ADDRESS"
                    ],
                )
            else:
                worked = True
            if not worked:
                print("Registrations are waiting but failed to send email")
                sys.exit(1)
            if not args.quiet:
                print("Emailed about waiting registrations")
        else:
            if not args.quiet:
                print("No registrations are waiting")
