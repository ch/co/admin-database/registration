def crsid_from_username(username):
    our_domain = "@cam.ac.uk"
    if username.endswith(our_domain):
        crsid = username[: -len(our_domain)]
    elif "@" in username:
        return "Nobody"
    else:
        crsid = username
    return crsid
