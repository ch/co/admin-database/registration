from chemaccmgmt import db
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


def get_sqlalchemy_db_conn():
    """
    Connect to database and return just the connection object
    """
    chemdb = db.ChemDB(username="starters_registration", use_pgpass=True)
    return chemdb.conn


engine = create_engine("postgresql://", creator=get_sqlalchemy_db_conn)

Session = scoped_session(sessionmaker(bind=engine))
