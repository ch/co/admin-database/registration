"""
Chemistry new starters registration`
"""

import datetime
import os

from flask import Flask, g, request

from chemistry_starters.sqlalchemy_db import Session

from .auth import crsid_from_username
from .version import __version__  # noqa F401

app = Flask(__name__)

app.config.from_object("chemistry_starters.default_settings")
app.config.from_pyfile("/etc/python3-chemstarters/starters.cfg", silent=True)
app.config.from_pyfile(
    os.path.expanduser("~/.config/python3-chemstarters/starters.cfg"), silent=True
)

if not app.debug:
    import logging
    from logging.handlers import SysLogHandler

    handler = SysLogHandler(address="/dev/log")
    handler.setLevel(logging.WARNING)
    handler.setFormatter(
        logging.Formatter(
            "%(asctime)s %(levelname)s %(name)s %(funcName)s %(pathname)s: %(message)s"
        )
    )
    app.logger.addHandler(handler)

# Want to make the current year available to all templates
@app.context_processor
def get_current_year():
    return {"current_year": datetime.date.today().year}


@app.before_request
def get_crsid():
    username = app.config.get("TEST_CRSID", None)
    if not username:
        username = request.environ.get("REMOTE_USER", "Nobody")
    g.crsid = crsid_from_username(username)


@app.teardown_request
def close_db_connections(exception):
    """
    Close the database connections
    """
    chemdb = getattr(g, "_database", None)
    if chemdb is not None:
        chemdb.conn.close()
    Session.remove()


class SecretKeyNotSet(Exception):
    pass


@app.before_request
def check_secret_key_set():
    if not app.debug:
        from .default_settings import SECRET_KEY

        if app.config["SECRET_KEY"] == SECRET_KEY:
            app.logger.error("Secret key has not been changed from default")
            raise SecretKeyNotSet


from chemistry_starters.views.static_content import static_content  # noqa E402

app.register_blueprint(static_content)

from chemistry_starters.views.error_pages import error_pages  # noqa E402

app.register_blueprint(error_pages)

from chemistry_starters.views.form_generation import form_generation  # noqa E402

app.register_blueprint(form_generation, url_prefix="/forms")

from chemistry_starters.views.form_examples import example_forms  # noqa E402

app.register_blueprint(example_forms, url_prefix="/forms/examples")

from chemistry_starters.views.person_role_details import (  # noqa E402
    person_role_details,
)

app.register_blueprint(person_role_details)

from chemistry_starters.views.safety_checklist import safety_checklist  # noqa E402

app.register_blueprint(safety_checklist)

from chemistry_starters.views.safety_training import safety_training  # noqa E402

app.register_blueprint(safety_training)

from chemistry_starters.views.starter_navigation import starter_navigation  # noqa E402

app.register_blueprint(starter_navigation)

from chemistry_starters.views.tokens import token_management  # noqa E402

app.register_blueprint(token_management, url_prefix="/tokens")
