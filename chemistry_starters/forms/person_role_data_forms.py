"""
The main registration forms, for personal and role data

These class objects need a database connection which lives on the g object to
set them up, so this file can't be imported until the application is running.
"""

from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, RadioField, TextAreaField, validators

try:
    from wtforms.fields import DateField, EmailField, TelField
except ImportError:
    from wtforms.fields.html5 import DateField, EmailField, TelField
from wtforms.widgets import TextInput

from chemistry_starters import database
from chemistry_starters.forms.widgets import (
    ChosenSelectField,
    ChosenSelectMultipleField,
    ChosenSelectOptionalField,
    EitherOr,
    InputRequiredOnlyIfOtherFieldEquals,
    StrippedStringField,
    trinary,
)
from chemistry_starters.roles import roles

from .common_fields import submit_box_factory, use_html_required_attr


def web_permission_box_factory():
    """
    Convenience function for generating website inclusion question
    """
    return RadioField(
        "Do you want to be included on publically accessible department web pages?",
        [validators.InputRequired(message="You must pick one of the options")],
        choices=[
            (
                False,
                """Include my details (name, email, department phone number,
                                    and photo) on department web pages""",
            ),
            (
                True,
                """Do not include my details on department web pages.
                                     If I pick this option I understand that I will not
                                     have a profile visible on department web pages""",
            ),
        ],
        coerce=trinary,
        render_kw={"required": use_html_required_attr},
    )


def university_card_box_factory():
    """
    Convenience function for generating checkboxes for University card
    """
    web_text = """Check this box if you already have a Cambridge University Card"""
    return BooleanField(web_text, [validators.Optional()])


def date_box_factory(instructions="Start date (YYYY-MM-DD)"):
    return DateField(
        instructions,
        [validators.InputRequired()],
        render_kw={
            "class_": "start-date-picker",
            "required": use_html_required_attr,
            "placeholder": "YYYY-MM-DD",
        },
        widget=TextInput(),
    )


def reception_contact_pref_factory():
    """
    Convenience function for generating question about contact details for Reception
    """
    return ChosenSelectField(
        (
            "What is the best contact method for an immediate response from you "
            "when you are in the building? "
            "For example we would use this when a visitor arrives for you at Reception."
        ),
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )


def reception_contact_pref_details_factory():
    """
    Convenience function for generating question about Other contact details
    """
    return StrippedStringField(
        (
            "If your contact preference for immediate responses is Other then please give details, "
            "for example if you are happy for us to use your personal mobile number for routine "
            "matters please repeat it here."
        ),
        [
            InputRequiredOnlyIfOtherFieldEquals(
                "contact_preference_id",
                3,
                message=(
                    "Must be filled in if you picked Other as "
                    "your method for immediate contact when in the building"
                ),
            )
        ],
        render_kw={"placeholder": "eg a mobile number"},
    )


def reception_contact_dept_phone_factory():
    """
    Convenience function for generating question about phone extensions
    """
    return ChosenSelectMultipleField(
        (
            "Department phone extension number(s) you will be reachable on. "
            "If your contact preference for immediate responses is Department "
            "Phone you must pick at least one extension number here."
        ),
        [
            InputRequiredOnlyIfOtherFieldEquals(
                "contact_preference_id",
                2,
                message=(
                    "You must pick at least one department phone number "
                    "you can usually be reached on when in the building"
                ),
            )
        ],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )


class BasicRegistrationForm(FlaskForm):
    """
    Abstract form containing most of the fields which are common to all categories

    The forms we actually use inherit from this class and add other fields. There are
    also a few fields which appear on multiple form types but ordering
    considerations mean we don't put them on this class, because we need them
    to appear after some of the per-category ones for usabilty reasons.
    """

    role = HiddenField("role")
    first_names = StrippedStringField("First name(s)", [validators.Length(max=32)])
    known_as = StrippedStringField(
        "Name you are known by if different from first names"
    )
    surname = StrippedStringField(
        "Surname",
        [validators.InputRequired(), validators.Length(max=32)],
        render_kw={"required": use_html_required_attr},
    )
    title_id = ChosenSelectOptionalField("Title", choices=[], coerce=int)
    # We use the textInput widget here so we can use jQuery's datepicker not
    # the HTML5 one, because some browers don't support that but I still want
    # the date validation and coercion properties of DateField
    date_of_birth = DateField(
        "Date of birth (YYYY-MM-DD)",
        [validators.InputRequired()],
        render_kw={
            "class_": "dob-date-picker",
            "required": use_html_required_attr,
            "placeholder": "YYYY-MM-DD",
        },
        widget=TextInput(),
    )
    nationality_id = ChosenSelectMultipleField(
        "Nationality",
        [validators.Optional()],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    gender_id = ChosenSelectField(
        "Gender", choices=[], coerce=int, render_kw={"required": use_html_required_attr}
    )
    previously_registered = BooleanField(
        "Have you been registered with us before? Leave blank if you have not."
    )
    dept_room_id = ChosenSelectMultipleField(
        "Department room numbers(s)",
        [validators.InputRequired()],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    home_address = TextAreaField(
        "Home address (sensitive)",
        [validators.InputRequired(), validators.Length(min=10)],
        render_kw={"required": use_html_required_attr},
    )
    home_phone_number = TelField(
        "Home phone number (sensitive)",
        [
            EitherOr(
                "mobile_number",
                message="One of home phone number and mobile number must be filled in",
            )
        ],
    )
    mobile_number = TelField(
        "Mobile phone number (sensitive)",
        [
            EitherOr(
                "home_phone_number",
                message="One of home phone number and mobile number must be filled in",
            )
        ],
    )
    email = EmailField(
        "Email Address",
        [validators.InputRequired(), validators.Email()],
        render_kw={"required": use_html_required_attr},
    )
    email2 = EmailField(
        "Email Address again",
        [
            validators.InputRequired(),
            validators.EqualTo("email", message="Email addresses must match"),
        ],
        render_kw={"required": use_html_required_attr},
    )
    crsid = StrippedStringField(
        "Cambridge CRSID (if known)",
        [validators.Length(max=7)],
        render_kw={"placeholder": "e.g. fjc55"},
    )
    emergency_contact_name_1 = StrippedStringField(
        "First emergency contact name (sensitive)",
        [validators.InputRequired()],
        render_kw={"required": use_html_required_attr},
    )
    emergency_contact_number_1 = TelField(
        "First emergency contact phone number (sensitive)",
        [validators.InputRequired()],
        render_kw={"required": use_html_required_attr},
    )
    emergency_contact_name_2 = StrippedStringField(
        "Second emergency contact name (sensitive)"
    )
    emergency_contact_number_2 = TelField(
        "Second emergency contact phone number (sensitive)"
    )

    def __init__(self, *args, **kwargs):
        super(BasicRegistrationForm, self).__init__(*args, **kwargs)
        self.reload_hid_lists()

    def reload_hid_lists(self):
        """
        Instance method to refresh hid lists from database
        """
        self.nationality_id.choices = database.get_hids("nationality")
        self.gender_id.choices = database.get_hids(
            "gender", null_option="Prefer not to say"
        )
        self.title_id.choices = database.get_hids("title", null_option="")
        self.dept_room_id.choices = database.get_hids("room")
        self.dept_telephone_id.choices = database.get_hids("phone")
        self.contact_preference_id.choices = database.get_hids("contact_preference")


class StaffRegistrationForm(BasicRegistrationForm):
    """
    Form used for Academic, Academic-related, and Assistant staff
    """

    start_date = date_box_factory()
    job_title = StrippedStringField("Job title")
    post_category_id = ChosenSelectField(
        "Post Category",
        [validators.InputRequired()],
        choices=[],
        default="sc-3",  # FIXME we should probably look up the 3
        render_kw={"required": use_html_required_attr},
    )
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    hide_from_website = web_permission_box_factory()
    already_has_university_card = university_card_box_factory()
    submitted = submit_box_factory()

    def reload_hid_lists(self):
        super(StaffRegistrationForm, self).reload_hid_lists()
        self.post_category_id.choices = database.get_hids(
            [
                roles[x]["post_choices_hid"]
                for x in roles
                if roles[x]["form_class"] == "StaffRegistrationForm"
            ][0],
            null_option="",
        )


class NonStaffRegistrationForm(BasicRegistrationForm):
    """
    Abstract form which adds fields needed for all categories other than StaffRegistrationForm ones
    """

    department_host_id = ChosenSelectField(
        "Department supervisor or host. If you are working "
        "with a research group select the head of the group.",
        [
            validators.InputRequired(),
            validators.NumberRange(min=1, message="This field is required"),
        ],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    college_id = ChosenSelectOptionalField(
        "Cambridge college",
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    hide_from_website = web_permission_box_factory()

    def reload_hid_lists(self):
        super(NonStaffRegistrationForm, self).reload_hid_lists()
        self.department_host_id.choices = database.get_hids(
            "supervisor", null_option=""
        )
        self.college_id.choices = database.get_hids("college", null_option="")


class ResearcherRegistrationForm(NonStaffRegistrationForm):
    """
    Form used for contract research staff
    """

    # no category, I guess we assume PDRA?
    start_date = date_box_factory()
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    hide_from_website = web_permission_box_factory()
    already_has_university_card = university_card_box_factory()
    submitted = submit_box_factory()


class PostGradRegistrationForm(NonStaffRegistrationForm):
    """
    Form used for postgraduates
    """

    crsid = StrippedStringField(
        "Cambridge CRSID (the part of your Cambridge email address before @cam.ac.uk)",
        [validators.InputRequired(), validators.Length(max=7)],
        render_kw={"required": use_html_required_attr, "placeholder": "e.g. fjc55"},
    )
    start_date = date_box_factory()
    post_category_id = ChosenSelectField(
        "Course type",
        [validators.InputRequired()],
        choices=[],
        render_kw={"required": use_html_required_attr},
    )
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    hide_from_website = web_permission_box_factory()
    already_has_university_card = university_card_box_factory()
    submitted = submit_box_factory()

    def reload_hid_lists(self):
        super(PostGradRegistrationForm, self).reload_hid_lists()
        self.post_category_id.choices = database.get_hids(
            [
                roles[x]["post_choices_hid"]
                for x in roles
                if roles[x]["form_class"] == "PostGradRegistrationForm"
            ][0],
            null_option="",
        )


class AllVisitorRegistrationForm(NonStaffRegistrationForm):
    """
    Abstract form used for visitor-alike categories

    With these we don't have an external source of data for their length of stay
    """

    start_date = date_box_factory()
    intended_end_date = date_box_factory("Intended finishing date (YYYY-MM-DD)")
    home_institution = StrippedStringField(
        "Home institution or company. If visiting from elsewhere in the "
        "University of Cambridge please put your department.",
        [validators.InputRequired()],
        render_kw={"required": use_html_required_attr},
    )
    # redefine question; wording is
    # aimed at visitors who have a short term address in Cambridge and a
    # permanent home elsewhere.
    home_address = TextAreaField(
        "Home address in Cambridge (sensitive)",
        [validators.InputRequired(), validators.Length(min=10)],
        render_kw={"required": use_html_required_attr},
    )
    # this field and next are not changed from BasicRegistrationForm but in order
    # to allow us to override the question for home_address and still have all
    # three questions appear together we need to define these two here too. This is repetitious
    # but setting up something more sophisticated like groups of fields will make
    # things much more complicated for little gain.
    home_phone_number = TelField(
        "Home phone number (sensitive)",
        [
            EitherOr(
                "mobile_number",
                message="One of home phone number and mobile number must be filled in",
            )
        ],
    )
    mobile_number = TelField(
        "Mobile phone number (sensitive)",
        [
            EitherOr(
                "home_phone_number",
                message="One of home phone number and mobile number must be filled in",
            )
        ],
    )


class VisitorRegistrationForm(AllVisitorRegistrationForm):
    """
    Form used for visitors
    """

    already_has_university_card = university_card_box_factory()
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    submitted = submit_box_factory()


class ErasmusRegistrationForm(AllVisitorRegistrationForm):
    """
    Form used for Erasmus students

    This is very similar to that for visitors, but they get a choice
    of post categories. The separation is useful to work around the quirks
    in the database schema but could be removed.
    """

    post_category_id = ChosenSelectField(
        "Category of Erasmus",
        [validators.InputRequired()],
        choices=[],
        render_kw={"required": use_html_required_attr},
    )
    already_has_university_card = university_card_box_factory()
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    submitted = submit_box_factory()

    def reload_hid_lists(self):
        super(ErasmusRegistrationForm, self).reload_hid_lists()
        self.post_category_id.choices = database.get_hids(
            [
                roles[x]["post_choices_hid"]
                for x in roles
                if roles[x]["form_class"] == "ErasmusRegistrationForm"
            ][0],
            null_option="",
        )


class EmbeddedStaffRegistrationForm(BasicRegistrationForm):
    """
    Form used for embedded company staff

    We need dates and a supervisor to satisfy schema constraints but otherwise nothing else
    """

    start_date = date_box_factory()
    intended_end_date = date_box_factory("Intended finishing date (YYYY-MM-DD)")
    department_host_id = ChosenSelectField(
        "Supervisor",
        [
            validators.InputRequired(),
            validators.NumberRange(min=1, message="This field is required"),
        ],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    contact_preference_id = reception_contact_pref_factory()
    dept_telephone_id = reception_contact_dept_phone_factory()
    contact_preference_details = reception_contact_pref_details_factory()
    already_has_university_card = university_card_box_factory()
    submitted = submit_box_factory()

    def reload_hid_lists(self):
        super(EmbeddedStaffRegistrationForm, self).reload_hid_lists()
        self.department_host_id.choices = database.get_hids(
            "supervisor", null_option=""
        )
