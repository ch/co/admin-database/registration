"""
Functions for commonly used fields
"""

from wtforms import BooleanField, validators

# It would be nice to use the browser checking on required fields, but that stops the user from
# saving a half-filled form so for the moment I'm turning that off
use_html_required_attr = False


def submit_box_factory():
    """
    Convenience function for generating standard submit checkboxes
    """
    submit_text = """I confirm that I am the person named above,
                     the data above is correct and complete,
                     and I wish to submit this form"""
    return BooleanField(
        submit_text,
        [validators.InputRequired()],
        render_kw={"required": use_html_required_attr},
    )
