"""
Defines simple static forms that do not require the database for setup
"""


from flask_wtf import FlaskForm
from wtforms import BooleanField, SelectField, TextAreaField, validators

from chemistry_starters.forms.widgets import StrippedStringField
from chemistry_starters.roles import roles

form_types = [(x, x) for x in roles]


class MifareApprovalForm(FlaskForm):
    """
    Form for allowing host to specify and approve MiFare access
    """

    mifare_areas = TextAreaField(
        "List additional Mifare areas to which access is required",
        [validators.InputRequired()],
    )


class SafetyInductionSignOffForm(FlaskForm):
    """
    For signing off that safety induction
    """

    safety_induction_signoff = BooleanField(
        "I carried out the safety induction and the information recorded is correct and complete",
        [validators.InputRequired()],
    )


class GetToken(FlaskForm):
    """
    For decoding tokens
    """

    token = StrippedStringField("Token or form link", [validators.InputRequired()])


class GetTokenParams(FlaskForm):
    """
    For creating custom tokens
    """

    uuid = StrippedStringField("UUID to encode", [validators.InputRequired()])
    role = SelectField("role", [validators.InputRequired()], choices=form_types)
    salt = SelectField(
        "salt",
        [validators.InputRequired()],
        choices=[
            (x, x) for x in ["form", "safety_checklist", "rendered_safety_checklist"]
        ],
    )
