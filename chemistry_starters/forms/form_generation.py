"""
Forms for generating registration forms
"""

from flask_wtf import FlaskForm
from wtforms import BooleanField, IntegerField, RadioField, TextAreaField, validators

from chemistry_starters.forms.widgets import (
    ChosenSelectOptionalField,
    InputRequiredOnlyIfOtherFieldEquals,
    StrippedStringField,
)
from chemistry_starters.roles import roles
from chemistry_starters.utils.form_generation import get_form_type_default

form_types = [(x, roles[x]["form_title"]) for x in roles]


class GenerateFormsForm(FlaskForm):
    """
    Form for allowing staff to generate forms for starters

    Choices for post_category_id must be set after instantiation
    """

    form_type = RadioField(
        "Type of form to generate",
        [validators.InputRequired()],
        choices=form_types,
        default=get_form_type_default,
    )
    post_category_id = ChosenSelectOptionalField(
        "If generating a visitor form select the visitor category",
        [InputRequiredOnlyIfOtherFieldEquals("form_type", "visitor", null_value="v-0")],
        choices=[],
    )
    starter_email = StrippedStringField(
        "Contact email address for starter", [validators.Optional(), validators.Email()]
    )


class GenerateBulkFormsForm(GenerateFormsForm):
    """
    Form for allowing staff to generate forms in bulk
    """

    starter_email = TextAreaField(
        "Contact email addresses, one per line", [validators.InputRequired()]
    )

    send_reminders = BooleanField("Send me email reminders about any incomplete forms")


class RecordFormInDatabaseForm(FlaskForm):
    """
    Form defining data saved when a new registration link is created

    This form is never presented to users, it's needed for
    passing data to utils.person_and_role_data.save_main_form()
    in the way it expects.
    """

    email = StrippedStringField(
        "Contact email address for starter", [validators.Email()]
    )

    notify_person_id = IntegerField(
        "Primary key of person to be notifed if form incomplete",
        [validators.NumberRange(min=1)],
    )

    form_role = StrippedStringField(
        "Role for form (determines which questions shown)", [validators.InputRequired()]
    )

    post_category_id = StrippedStringField("Initial post category")
