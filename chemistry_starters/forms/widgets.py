"""
Custom form building classes extending WTForms classes
"""
from wtforms import SelectField, SelectMultipleField, StringField
from wtforms.validators import AnyOf, EqualTo, InputRequired, NoneOf, ValidationError

######################################################
# Custom Fields which use the Chosen jQuery widget
######################################################
# From https://www.julo.ch/blog/wtforms-chosen/


def _add_chosen_class(cssclass, kwargs):
    if "render_kw" in kwargs:
        if "class" in kwargs["render_kw"]:
            if cssclass not in kwargs["render_kw"]["class"].split():
                kwargs["render_kw"]["class"] += " {}".format(cssclass)
        else:
            kwargs["render_kw"]["class"] = cssclass
    else:
        kwargs["render_kw"] = {"class": cssclass}


class ChosenSelectField(SelectField):
    """
    A Select field which gets rendered with the class to use Chosen jquery widget
    """

    def __init__(self, *args, **kwargs):
        _add_chosen_class("chosen-select", kwargs)
        super(ChosenSelectField, self).__init__(*args, **kwargs)


class ChosenSelectOptionalField(SelectField):
    """
    A Select field which gets rendered with the class to use Chosen jquery widget

    This one uses the Chosen variation where filling the field is optional
    """

    def __init__(self, *args, **kwargs):
        _add_chosen_class("chosen-select-deselect", kwargs)
        super(ChosenSelectOptionalField, self).__init__(*args, **kwargs)


class ChosenSelectMultipleField(SelectMultipleField):
    """
    A multiple Select field which gets rendered with the class to use Chosen jquery widget
    """

    def __init__(self, *args, **kwargs):
        _add_chosen_class("chosen-select", kwargs)
        super(ChosenSelectMultipleField, self).__init__(*args, **kwargs)


######################################################


class EitherOr(EqualTo):
    """
    Custom validator for checking we have one or the other
    of two fields
    """

    def __call__(self, form, field):
        try:
            other = form[self.fieldname]
        except KeyError:
            raise ValidationError(
                field.gettext("Invalid field name '%s'.") % self.fieldname
            )
        if (not field.data) and (not other.data):
            d = {"other_name": self.fieldname, "my_name": field.name}
            message = self.message
            if message is None:
                message = field.gettext(
                    "Either {my_name} or {other_name} must be filled in."
                )

            raise ValidationError(message.format(**d))


class InputRequiredOnlyIfOtherFieldEquals(object):
    """
    Field must be filled if another field has a particular value
    """

    def __init__(self, fieldname, value, null_value=None, message=None):
        self.fieldname = fieldname
        self.value = value
        if message:
            self.message = message
        else:
            self.message = f"Input required when {fieldname} has {value}"
        self.null_value = null_value

    def __call__(self, form, field):
        try:
            other = form[self.fieldname]
        except KeyError:
            raise ValidationError(
                field.gettext("Invalid field name '%s'.") % self.fieldname
            )
        if other.data == self.value:
            # call the regular InputRequired validator first
            ir = InputRequired(message=self.message)
            ir(form, field)
            # now exclude any special value representing null
            if self.null_value:
                nv = NoneOf(
                    [self.null_value], message="A visitor category must be selected"
                )
                nv(form, field)
        else:
            if self.null_value:
                nv = AnyOf(
                    [self.null_value],
                    message="No visitor category should be selected for this form type",
                )
                nv(form, field)


class StrippedStringField(StringField):
    """
    Custom field which strips whitespace from strings
    """

    def __init__(self, *args, **kwargs):
        def strip_string(x):
            if x and hasattr(x, "strip"):
                return x.strip()
            else:
                return x

        kwargs["filters"] = kwargs.get("filters", ()) + (strip_string,)
        super().__init__(*args, **kwargs)


def trinary(input):
    """
    Coerce browser field data to suitable value for nullable boolean

    Because the brower only sends us strings or nothing (None) and all strings but the empty string
    are truthy we need a special value "False" to mark False data
    """
    if input is None:
        return input
    if input == "False":
        return False
    else:
        return bool(input)
