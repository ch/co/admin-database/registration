"""
The safety registration forms

"""

from flask_wtf import FlaskForm
from wtforms import BooleanField, RadioField, SelectField, validators

from chemistry_starters import database
from chemistry_starters.forms.widgets import ChosenSelectField

from .common_fields import submit_box_factory, use_html_required_attr

safety_group_expectations_options = [
    """
    I confirm that I have received my Group Expectations Document.
    I have discussed its contents with the person carrying out my safety induction.
    I am registered for a course that is scrutinized by oral examination (viva)
    and am aware of the University Code of Practice.
    """,
    """
    I confirm that I have received my Group Expectations Document.
    I have discussed its contents with the person carrying out my safety induction.
    """,
    """
    I am not in a research group, so do not receive a Group Expectations Document
    """,
    """
    I am a member of academic staff
    """,
]

safety_group_lab_management_options = [
    """
    I have been provided with a copy of the most recent Laboratory Group Management & Safety Plan
    and instructed to read as appropriate
    """,
    """
    I am not in a research group, so do not receive a Laboratory Group Management & Safety Plan
    """,
    """
    I am a member of academic staff
    """,
]


def checklist_generator(item, optional=False):
    """
    Factory function for generating safety checklist items
    """
    if optional:
        return BooleanField(item)
    else:
        return BooleanField(
            item,
            [validators.InputRequired()],
            render_kw={"required": use_html_required_attr},
        )


class SafetyChecklist(FlaskForm):
    """
    Safety handbook checklist and safety induction checklist form

    Replaces the department Green Form and White Form
    """

    safety_handbook_part_1 = checklist_generator(
        """I have obtained a copy of the current Departmental Safety Handbook
        or have viewed online. I have read Part 1."""
    )
    safety_handbook_part_2_chemical_safety_information_and_guidelines = (
        checklist_generator("Chemical safety information and guidelines", optional=True)
    )
    safety_handbook_part_2_biological_safety = checklist_generator(
        "Biological safety", optional=True
    )
    safety_handbook_part_2_ionising_radiation = checklist_generator(
        "Ionising Radiation", optional=True
    )
    safety_handbook_part_2_non_ionising_radiation_lasers = checklist_generator(
        "Non-Ionising Radiation Lasers", optional=True
    )
    safety_handbook_part_2_non_ionising_radiation_uv = checklist_generator(
        "Non-Ionising Radiation UV", optional=True
    )
    safety_handbook_part_2_non_ionising_radiation_em_fields_nmr = checklist_generator(
        "Non-Ionising Radiation Electromagnetic fields (NMR)", optional=True
    )
    safety_handbook_part_2_compressed_gas_cylinders = checklist_generator(
        "Compressed Gas Cylinders", optional=True
    )
    safety_handbook_part_2_liquefied_cryogenic_gases = checklist_generator(
        "Liquefied/Cryogenic Gases", optional=True
    )
    safety_handbook_part_2_pressure_systems_and_vessels = checklist_generator(
        "Pressure Systems and Vessels", optional=True
    )
    safety_handbook_part_2_glassware_and_sharps = checklist_generator(
        "Glassware and Sharps", optional=True
    )
    safety_handbook_part_2_oil_baths = checklist_generator("Oil Baths", optional=True)
    safety_handbook_part_2_lifting_equipment = checklist_generator(
        "Lifting Equipment", optional=True
    )
    safety_handbook_part_2_safe_use_of_fume_cupboards = checklist_generator(
        "Safe use of Fume Cupboards", optional=True
    )
    safety_handbook_part_2_agree_code_of_practice = checklist_generator(
        "I agree to comply with the codes of practice detailed within the Safety Handbook.",
        optional=False,
    )
    safety_induction_person_signing_off_id = ChosenSelectField(
        "The person who carried out my safety induction is",
        [
            validators.InputRequired(),
            validators.NumberRange(min=1, message="This field is required"),
        ],
        choices=[],
        coerce=int,
        render_kw={"required": use_html_required_attr},
    )
    safety_induction_agree_discussed_group_expectations_and_uni_code_of_practice = (
        RadioField(
            "Pick the Research Group Expectations option which applies to you",
            [validators.InputRequired(message="You must pick one of the options")],
            choices=[
                (y, y)
                for y in [
                    " ".join(x.split()) for x in safety_group_expectations_options
                ]
            ],
            render_kw={"required": use_html_required_attr},
        )
    )
    safety_induction_agree_discussed_with_supervisor = checklist_generator(
        " ".join(
            """I have discussed the general nature of my work/research work with the person carrying
           out my safety induction, named above. Written material has been provided or indicated.
           I have read this material, and taken account of the potential hazards of the work
           which are noted within this material.""".split()
        )
    )
    safety_induction_spectacles = SelectField(
        "I have collected my safety spectacles from Stores",
        [
            validators.InputRequired(),
            validators.Regexp(
                "^(Yes|N/A)$", message="Yes or N/A are acceptable answers"
            ),
        ],
        choices=[
            ("No", "No"),
            ("Yes", "Yes"),
            ("N/A", "N/A - these are not required for my research"),
        ],
        default="No",
    )
    safety_induction_agree_seen_policy_personal = checklist_generator(
        """My attention has been drawn to the Health and Safety policy
        and I understand my responsibility for my personal safety"""
    )
    safety_induction_agree_seen_group_management_plan = RadioField(
        "Pick the Laboratory Safety Management option which applies to you",
        [validators.InputRequired(message="You must pick one of the options")],
        choices=[
            (y, y)
            for y in [" ".join(x.split()) for x in safety_group_lab_management_options]
        ],
        render_kw={"required": use_html_required_attr},
    )
    safety_induction_agree_seen_fire_discovery_evacuation_instructions = checklist_generator(
        """I have been provided with instructions about Fire Discovery
        and the Evacuation Drill including details about alarm testing (day/time)"""
    )
    safety_induction_agree_seen_firstaider_procedure = checklist_generator(
        "I have read and understood the procedure for summoning a First Aider"
    )
    safety_induction_agree_seen_hazard_notification_and_emergency_procedures = checklist_generator(
        "The procedure for hazard notification and emergency procedures has been explained to me"
    )
    safety_induction_agree_seen_near_misses_procedure = checklist_generator(
        "I have been advised of the procedures for reporting accidents and near-misses"
    )
    safety_induction_agree_aware_of_work_area_safety_issues = checklist_generator(
        """Any inherent safety features and safety issues to be aware of in the work area
        have been pointed out to me"""
    )
    safety_induction_agree_aware_of_safe_operating_and_good_housekeeping = checklist_generator(
        """The need for `Safe Operating Procedures' and `Good Housekeeping'
        has been explained to me"""
    )
    safety_induction_agree_aware_of_waste_disposal_procedures = checklist_generator(
        """Local waste disposal procedures and details regarding segregation of waste
        have been explained to me"""
    )
    safety_induction_agree_aware_of_areas_where_food_can_be_consumed = (
        checklist_generator(
            """I have been advised where food and drink can and cannot be consumed"""
        )
    )
    safety_induction_agree_aware_of_smoking_vaping_areas = checklist_generator(
        """I have been advised where smoking/vaping is permitted"""
    )
    safety_induction_agree_aware_of_mifare_and_key_system = checklist_generator(
        """The provision and use of the Mifare security card, keys, etc.
        has been discussed with me"""
    )
    safety_induction_agree_aware_of_out_of_hours_working_rules = checklist_generator(
        """The Departmental rules about out of hours working have been explained to me"""
    )
    safety_induction_agree_aware_of_location_of_documentation = checklist_generator(
        """I have been shown the location of Risk Assessments and method statements (RAMS),
        protocols or safe operating procedures, and Safety Data Sheets"""
    )
    safety_induction_agree_aware_of_safety_committee = checklist_generator(
        """I have been advised on representation on the Department's Safety Committee"""
    )
    safety_induction_agree_aware_of_safety_staff = checklist_generator(
        """I have been advised of the names and contact details of the Department Safety Officer
        and Department Safety Technician"""
    )

    safety_submitted = submit_box_factory()

    def __init__(self, *args, **kwargs):
        super(SafetyChecklist, self).__init__(*args, **kwargs)
        self.safety_induction_person_signing_off_id.choices = database.get_hids(
            "supervisor", null_option=""
        )


class SafetyChecklistWithTraining(SafetyChecklist):
    """
    A version of the saftey form which requires a training declaration
    """

    safety_training_declaration = submit_box_factory()
