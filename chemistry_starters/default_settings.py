"""
Default settings for the registration app
"""
DB_SCHEMA = "registration"

FORM_TABLE = "form"

SECRET_KEY = "super-secret-key"

CURRENT_CRSIDS_VIEW = "current_people_crsids"

FORM_GENERATION_ACL_VIEW = "crsids_allowed_to_generate_forms"

COMPLETED_FORMS_VIEW = "completed_form_summary"

ROLES_VIEW = "role_data"

SAFETY_TRAINING_VIEW = "safety_training"

REMINDERS_VIEW = "incomplete_form_reminders"

HR_EMAIL_ADDRESSES = ["registration@ch.cam.ac.uk"]
PHOTOGRAPHY_EMAIL_ADDRESSES = ["photography@ch.cam.ac.uk"]
SAFETY_TRAINING_EMAIL_ADDRESSES = ["postgraduate.education@ch.cam.ac.uk"]
FROM_EMAIL_ADDRESS = "registration@ch.cam.ac.uk"
ENVELOPE_FROM_ADDRESS = "root@ch.cam.ac.uk"

FORM_LIFETIME_DAYS = 14

REMINDER_INTERVAL = int(FORM_LIFETIME_DAYS / 2)

SAFETY_HANDBOOK_FILENAME = "safety-handbook.pdf"
